import React, { Component } from "react";
import './styles.scss';
interface LoadingProps {
    showLoading : boolean
}

interface LoadingState {
    isShowLoading: boolean
}

class Loading extends React.Component<LoadingProps, LoadingState> {
    constructor(props: LoadingProps) {
        super(props)
        this.state = {
            isShowLoading: false
        }
    }

    componentDidMount() {
        this.setState({
            isShowLoading : this.props.showLoading
        })
    }

    UNSAFE_componentWillReceiveProps(nextProps: LoadingProps) {
        if (nextProps) {
            this.setState({
                isShowLoading: nextProps.showLoading
            })
        }
    }

    render() {
        return (

            this.state.isShowLoading
                ? (
                    <div className='wrap-loading'>
                        <div className="lds-default">
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                )
                : null
        )
    }
}

export default Loading;