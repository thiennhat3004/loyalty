import React from 'react';
import './styles.scss';

interface Props {
    tableHead: any,
    tableBody: any,
    handleAction: any
}

interface State {

}

class Table extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {

        }
    }

    actionMaster = (value: any, i: any) => {
        this.props.handleAction(value, i);
    }

    showMenuActionMaster = (e: any) => {
        const evt = e.currentTarget.nextSibling;
        if (evt.classList.contains('show-menu')) {
            evt.classList.remove('show-menu');
        } else {
            evt.classList.add('show-menu');
        }
    }

    hideMenuActionMaster = (e: any) => {
        e.currentTarget.classList.remove('show-menu');
    }

    itemInTable = (value: any) => {
        return value.map((p: any, k: any) => {
            if (!p.action) {
                return (
                    <td key={k}>
                        {p.text}
                    </td>
                );
            } else {
                return (
                    <td key={k}>
                        <div >
                            <div className='wrap-multi-dot' onClick={(e) => this.showMenuActionMaster(e)}>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                            <div className='menu-action-master' onClick={(e) => this.hideMenuActionMaster(e)}>
                                <ul>
                                    {p.action.map((option: any, i: any) => (
                                        <li key={i} onClick={(e) => this.actionMaster(value, i)}>
                                            {option}
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </td>
                    
                )
            }

        });
    }
    render() {
        const { tableHead, tableBody } = this.props;
        return (
            <div >
                <table id='myTable'>
                    {tableHead !== undefined ? (
                        <thead >
                            <tr className='header'>
                                {tableHead.map((prop: any, key: any) => {
                                    return (
                                        <th
                                            
                                            key={key}
                                        >
                                            {prop}
                                        </th>
                                    );
                                })}
                            </tr>
                        </thead>
                    ) : null}
                    <tbody>
                        {tableBody.map((value: any, key: any) => {
                            return (
                                <tr key={key}>
                                    {this.itemInTable(value)}
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

}




export default Table;
