import React, { Component } from 'react';
import './styles.scss';
import { connect } from 'react-redux';

interface Props {
    history: {
        push(url: string): any
    },
    location: any
}

interface State {
    itemALls: any,
    items: any
}

class LeftMenu extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            itemALls: [
                { title: 'Dashboard', url: '/super-admin/dashboard', icon: '/img/action/dashboard_24px.svg', active: true, role: 2 },
                { title: 'List master', url: '/super-admin/list-master', icon: '/img/action/leaderboard_24px.svg', active: false, role: 2 },
                { title: 'My Topics', url: '/my-topic', icon: '/img/action/whatshot_24px.svg', active: false, role: 2 },
                { title: 'Playlog Search', url: '/playlog-search', icon: '/img/action/playlog_search_24px.svg', active: false, role: 2 },
                { title: '詳細検索', url: '/detailed-search', icon: '/img/action/research_24px.svg', active: false, role: 2 },
                { title: '納品', url: '/admin-delivery', icon: '/img/action/delivery_24px.svg', active: false, role: 1 },
                { title: '配信', url: '/admin-news', icon: '/img/action/mail_24px.svg', active: false, role: 1 },
                { title: '納品', url: '/delivery', icon: '/img/action/delivery_24px.svg', active: false, notification: true, role: 0 },
            ],
            items: []
        }
    }

    onActiveNav = (itemAll: any, item: any) => {
        const { history } = this.props;
        itemAll.forEach((i: any, k: any) => {
            i.active = false;
        })
        if (item.url) {
            item.active = true;
            history.push(`${item.url}`);
        }
    }

    findItemActive = (it: any) => {
        this.state.itemALls.forEach((obj: any) => {
            if (it.includes(obj.url)) {
                obj.active = true;
            } else {
                obj.active = false;
                if (it.includes(obj.urlChild)) {
                    obj.active = true;
                }

            }
        });
    }

    UNSAFE_componentWillMount() {
        this.findItemActive(this.props.location.pathname);
    }

    UNSAFE_componentWillReceiveProps = (nextProps: any) => {
        // this.findItemActive(nextProps.location.pathname);

        // if (nextProps.statusNotification == ACTIONS_NOTIFICATION.GET_NOTIFICATION_SUCCESS) {
        //     this.setState({
        //         notification: nextProps.responseNotification.delivery
        //     })
        // }
        this.setState({
            items: this.state.itemALls.filter((o: any) => {
                return o
            })
        })

    }

    render() {
        return (
            <div className="left-menu">
                <div className="left-menus">
                    <div className="content">
                        <img src="/img/action/home.svg" alt="" />
                    </div>

                    {this.state.items.map((value: any, key: any) => {
                        return (
                            <div
                                key={key.toString()}
                                className="content"
                                onClick={() => this.onActiveNav(this.state.items, value)}
                            >
                                <div className={`item ${value.notification ? 'notification' : ''} ${value.active === true ? 'active' : ''}`}>
                                    <div className="icon">
                                        <img src={value.icon} alt="" />
                                        {/* {value.notification && this.state.notification ?
                                                <div className="ic-notification">{this.state.notification}</div> : ''} */}
                                    </div>
                                    <p className="item-name">{value.title}</p>
                                </div>
                            </div>
                        )
                    })}

                </div>
            </div>
        )
    }
}

const mapStateToProps = ({ }) => {
    return {
        // responseNotification: notification.response,
        // statusNotification: notification.status,
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(LeftMenu);
