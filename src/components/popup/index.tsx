import React, { useState } from 'react';
import './styles.scss';

interface Props {
    title: string,
    content: string,
    showModal: boolean,
    isNotification: boolean,
    acceptAction: any,
    textAccept: string,
    onCloseModal: any
}

interface State {
    indexModal: any,
    title: string,
    content: string,
    isShowModal: boolean,
    isNotification: boolean,
    textAccept: string
}

class Modal extends React.Component<Props, State>  {

    constructor(props: Props) {
        super(props);
        this.state = {
            indexModal: 0,
            title: '',
            content: '',
            isShowModal: false,
            isNotification: true,
            textAccept: ''
        }
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({
            title: nextProps.title,
            content: nextProps.content,
            isShowModal: nextProps.showModal,
            isNotification: nextProps.isNotification,
            textAccept: nextProps.textAccept
        })
        
    }

    clickOutPopup(e: any) {
        const selectPopup = document.getElementById('popup');
        if (selectPopup) {
            if (!selectPopup.contains(e.target)) {
                this.props.onCloseModal();
            }
        }
        
    }

    acceptAction() {
        this.setState({
            isShowModal: false
        });
        this.props.acceptAction();
        this.props.onCloseModal();
    }


    onCloseModal() {
        this.props.onCloseModal();
    }

    render() {
        return(
                this.state.isShowModal
                    ? <div className='wrap-popup' onClick={(e) => this.clickOutPopup(e)}>
                        <div className='popup' id='popup'>
                            <h4>{ this.state.title || 'Notifications' }</h4>
                            <p>{ this.state.content || 'Are you sure ?' }</p>
                            <div className='group-btn-popup'>
                                {
                                    !this.state.isNotification
                                        ? <button onClick={() => this.onCloseModal()} className='cancel'>Cancel</button>
                                        : null
                                }
                                {
                                    this.state.isNotification
                                        ? <button onClick={() => this.onCloseModal()}>Close</button>
                                        :  <button onClick={() => this.acceptAction()}>{this.state.textAccept || 'Confirm'}</button>
                                }
                                
                                
                            </div>
                        </div>
        
                    </div>
                    : null
        )
    }
    
}

export default Modal;