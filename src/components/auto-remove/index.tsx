import React, { Fragment }  from "react";
import './styles.scss';

interface Props {
    
}

interface MyState  {
}

class AutoRemove extends React.Component<Props, MyState> {
    
    constructor(props: Props) {
        super(props);
        this.state = {
        }
    }

    

    render() {
        return(
            <Fragment>
                <div id="wrap-auto-logout">
                {/* <span id='seconds'></span> */}
                    <div id="halfclip">
                        <div className="halfcircle" id="clipped">
                        </div>
                    </div>
                    <div className="halfcircle" id="fixed">
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default AutoRemove;