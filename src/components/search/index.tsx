import React, { useState } from 'react';
import './styles.scss';

interface Props {
}

interface State {
}

class Search extends React.Component<Props, State>  {

    constructor(props: Props) {
        super(props);
        this.state = {
        }
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({
        })

    }

    clickOutPopup(e: any) {
    }



    render() {
        return (
            <div className='search'>
                <input placeholder='Search' type='text' />
                <img alt='search' src='/images/ic_search_svg.svg' />
            </div>
        )
    }

}

export default Search;