const host = {
    local: 'http://jsonplaceholder.typicode.com/users',
    server: 'https://api.ashirobo.i3design.dev/',
    serverTest: 'http://127.0.0.1:8000/',
    user_host: 'localhost:3000',
  };
  
  export { host };
  