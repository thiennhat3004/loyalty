import { callApi } from '../common/services/baseServices';
import { host } from '../environment';

const ACTIONS = {
    //   GET_JSON: 'GET_JSON',
    //   SHOW_LOADING: 'SHOW_LOADING',
    //   LOGIN_PROGRESS: 'LOGIN_PROGRESS',
    SIGNUP_SUCCESS: 'SIGNUP_SUCCESS',
    SIGNUP_ERROR: 'SIGNUP_ERROR',
};

const updateStatus = (type, data) => ({
    type,
    data,
});

const signup = (data) => {
    const config = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        },
        body: JSON.stringify({
            //   grant_type: 'password',
            //   client_id: '2',
            //   client_secret: 'r1FcHzRdcNriP5lYxNSu2iiV9l1B9e4zOQASG9ub',
            name: data.name,
            email: data.email,
            password: data.password,
            //   refresh_token: '',
        }),
    };
    return (dispatch) => {
        // updateStatus(ACTIONS.LOGIN_PROGRESS, true);
        callApi(`${host.serverTest}api/auth/register`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.SIGNUP_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.SIGNUP_ERROR, err));
            });
    };
};

export { signup, ACTIONS };
