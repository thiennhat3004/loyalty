import { callApi } from '../common/services/baseServices';
import { host } from '../environment';
import { ACTIONS } from './contants/contants';

const updateStatus = (type, data) => ({
  type,
  data,
});

const sendForgotPassword = (data) => {
  const config = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json',
      'X-CSRF-TOKEN': '',
    },
    body: JSON.stringify({
      email: data.email, 
    }),
  };
  return (dispatch) => {
    callApi(`${host.serverTest}api/auth/forgot/password`, config,
      (res) => {
          console.log(res);
          
        dispatch(updateStatus(ACTIONS.SEND_FORGOT_PASSWORD_SUCCESS, res));
      },
      (err) => {
        dispatch(updateStatus(ACTIONS.SEND_FORGOT_PASSWORD_ERROR, err));
      });
  };
};

export { sendForgotPassword };
