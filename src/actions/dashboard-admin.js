import { callApi } from '../common/services/baseServices';
import { host } from '../environment';
import { ACTIONS } from './contants/contants';

const updateStatus = (type, data) => ({
    type,
    data,
});

const getNameDevice = (data) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        },
        body: JSON.stringify({
            token_address: data.token_address
        }),
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/admin/device/name`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.GET_NAME_DEVICE_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_NAME_DEVICE_ERROR, err));
            });
    };
};

const updateNameDevice = (data) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        },
        body: JSON.stringify({
            device_id : data.device_id,
            name: data.name
        }),
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/admin/device`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.UPDATE_NAME_DEVICE_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.UPDATE_NAME_DEVICE_ERROR, err));
            });
    };
};

const getProgramOfMaster = () => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/admin/program`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.GET_PROGRAM_MASTER_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_PROGRAM_MASTER_ERROR, err));
            });
    };
};

const deleteProgram = (id) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/admin/program/delete/${id}`, config,
            (res) => {
                console.log(res);
                
                dispatch(updateStatus(ACTIONS.DELETE_PROGRAM_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.DELETE_PROGRAM_ERROR, err));
            });
    };
};


export { getNameDevice, updateNameDevice, getProgramOfMaster, deleteProgram };
