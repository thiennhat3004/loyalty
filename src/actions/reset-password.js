import { callApi } from '../common/services/baseServices';
import { host } from '../environment';
import { ACTIONS } from './contants/contants';


const updateStatus = (type, data) => ({
  type,
  data,
});

const resetPassword = (data) => {
  const config = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json',
      'X-CSRF-TOKEN': '',
    },
    body: JSON.stringify({
      email: data.email, 
      password: data.password, 
      token: data.token
    }),
  };
  return (dispatch) => {
    callApi(`${host.serverTest}api/auth/change/password`, config,
      (res) => {
        console.log(res);
        
        dispatch(updateStatus(ACTIONS.RESET_PASSWORD_SUCCESS, res));
      },
      (err) => {
        console.log(err);
        
        dispatch(updateStatus(ACTIONS.RESET_PASSWORD_ERROR, err));
      });
  };
};

export { resetPassword };
