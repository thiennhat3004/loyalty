import { callApi } from '../common/services/baseServices';
import { host } from '../environment';
import { ACTIONS } from './contants/contants';

const updateStatus = (type, data) => ({
    type,
    data,
});

const getLayoutScreen = (id) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/user/screen/${id}`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.GET_SCREEN_DEVICE_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_SCREEN_DEVICE_ERROR, err));
            });
    };
};

const checkNumberPhone = (body) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        },
        body: JSON.stringify(body)
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/user/check-phone`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.CHECK_PHONE_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.CHECK_PHONE_ERROR, err));
            });
    };
};



export { getLayoutScreen, checkNumberPhone };
