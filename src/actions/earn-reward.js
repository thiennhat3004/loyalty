import { callApi } from '../common/services/baseServices';
import { host } from '../environment';
import { ACTIONS } from './contants/contants';

const updateStatus = (type, data) => ({
    type,
    data,
});

const getScoreUserOfGroup = (body) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/user/score/${body.user_id}/${body.group_id}`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.GET_SCORE_USER_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_SCORE_USER_ERROR, err));
            });
    };
};

const getAllPackageDevice = (id) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/user/package/${id}`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.GET_PACKAGE_DEVICE_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_PACKAGE_DEVICE_ERROR, err));
            });
    };
};


const resultFinallyReward = (body) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/user/confirm-reward/${body.user_id}/${body.group_id}`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.GET_REWARD_FINALLY_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_REWARD_FINALLY_ERROR, err));
            });
    };
};


const resetScoreReward = (body) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        },
        body: JSON.stringify(body)
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/user/reset-score`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.RESET_SCORE_USER_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.RESET_SCORE_USER_ERROR, err));
            });
    };
};


export { getScoreUserOfGroup, getAllPackageDevice, resultFinallyReward, resetScoreReward };
