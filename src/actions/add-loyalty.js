import { callApi } from '../common/services/baseServices';
import { host } from '../environment';
import { ACTIONS } from './contants/contants';

const updateStatus = (type, data) => ({
    type,
    data,
});


const getListScreenDevice = () => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/admin/screen/device`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.GET_LIST_SCREEN_DEVICE_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_LIST_SCREEN_DEVICE_ERROR, err));
            });
    };
};


const getListScreenName = () => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/admin/screen/name`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.GET_LIST_SCREEN_NAME_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_LIST_SCREEN_NAME_ERROR, err));
            });
    };
};


const addProgram = (data) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        },
        body: JSON.stringify({
            program_name: data.program_name,
            description: data.description,
            how_reward: data.how_reward,
            time_checkin: data.time_checkin,
            business_name: data.business_name,
            package: data.package,
            screens: data.screens
        }),
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/admin/program/add`, config,
            (res) => {
                console.log(res);
                dispatch(updateStatus(ACTIONS.ADD_PROGRAM_SUCCESS, res));
            },
            (err) => {
                console.log(err);
                dispatch(updateStatus(ACTIONS.ADD_PROGRAM_ERROR, err));
            });
    };
};


export { addProgram, getListScreenName, getListScreenDevice };
