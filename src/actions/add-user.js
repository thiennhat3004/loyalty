import { callApi } from '../common/services/baseServices';
import { host } from '../environment';
import { ACTIONS } from './contants/contants';

const updateStatus = (type, data) => ({
    type,
    data,
});


const addUser = (body) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        },
        body: JSON.stringify(body)
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/user/add`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.ADD_USER_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.ADD_USER_ERROR, err));
            });
    };
};



export { addUser };
