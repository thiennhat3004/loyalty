import { callApi } from './../common/services/baseServices';
import { host } from './../environment';
import { ACTIONS } from './contants/contants';

const updateStatus = (type, data) => ({
    type,
    data,
});

const getProfile = () => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/auth/profile`, config,
            (res) => {
                console.log(res);
                
                dispatch(updateStatus(ACTIONS.GET_PROFILE_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_PROFILE_ERROR, err));
            });
    };
};

const logout = () => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${ACCESS_TOKEN}`,
        accept: 'application/json',
        'X-CSRF-TOKEN': '',
      },
    };
    return (dispatch) => {
      callApi(`${host.serverTest}api/auth/logout`, config,
        (res) => {
          console.log(res);
          
          dispatch(updateStatus(ACTIONS.LOGOUT_SUCCESS, res));
        },
        (err) => {
          console.log(err);
          
          dispatch(updateStatus(ACTIONS.LOGOUT_ERROR, err));
        });
    };
  };


export { getProfile, logout };
