import { callApi } from '../common/services/baseServices';
import { host } from '../environment';
import { ACTIONS } from './contants/contants';

const updateStatus = (type, data) => ({
  type,
  data,
});

const login = (data) => {
  const config = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json',
      'X-CSRF-TOKEN': '',
    },
    body: JSON.stringify({
    //   grant_type: 'password',
    //   client_id: '2',
    //   client_secret: 'r1FcHzRdcNriP5lYxNSu2iiV9l1B9e4zOQASG9ub',
      email: data.email, 
      password: data.password, 
      address_device: data.address_device,
    }),
  };
  return (dispatch) => {
    // updateStatus(ACTIONS.LOGIN_PROGRESS, true);
    callApi(`${host.serverTest}api/auth/login`, config,
      (res) => {
        dispatch(updateStatus(ACTIONS.LOGIN_SUCCESS, res));
      },
      (err) => {
        dispatch(updateStatus(ACTIONS.LOGIN_ERROR, err));
      });
  };
};

export { login };
