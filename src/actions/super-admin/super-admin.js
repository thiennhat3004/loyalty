import { callApi } from '../../common/services/baseServices';
import { host } from '../../environment';

const ACTIONS = {
    // GET_JSON: 'GET_JSON',
    // SHOW_LOADING: 'SHOW_LOADING',
    // LOGIN_PROGRESS: 'LOGIN_PROGRESS',
    GET_PROFILE_SUCCESS: 'GET_PROFILE_SUCCESS',
    GET_PROFILE_ERROR: 'GET_PROFILE_ERROR',
};

const updateStatus = (type, data) => ({
    type,
    data,
});

const getProfile = () => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/auth/profile`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.GET_PROFILE_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_PROFILE_ERROR, err));
            });
    };
};

export { getProfile, ACTIONS };
