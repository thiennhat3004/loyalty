import { callApi } from '../../common/services/baseServices';
import { host } from '../../environment';

const ACTIONS = {
    // GET_JSON: 'GET_JSON',
    // SHOW_LOADING: 'SHOW_LOADING',
    // LOGIN_PROGRESS: 'LOGIN_PROGRESS',
    GET_LIST_MASTER_SUCCESS: 'GET_LIST_MASTER_SUCCESS',
    GET_LIST_MASTER_ERROR: 'GET_LIST_MASTER_ERROR',
    DELETE_MASTER_SUCCESS: 'DELETE_MASTER_SUCCESS',
    DELETE_MASTER_ERROR: 'DELETE_MASTER_ERROR',
    SET_ROLE_MASTER_SUCCESS: 'SET_ROLE_MASTER_SUCCESS',
    SET_ROLE_MASTER_ERROR: 'SET_ROLE_MASTER_ERROR'
};

const updateStatus = (type, data) => ({
    type,
    data,
});

const getListMaster = () => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        }
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/admin/master`, config,
            (res) => {
                dispatch(updateStatus(ACTIONS.GET_LIST_MASTER_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_LIST_MASTER_ERROR, err));
            });
    };
};

const deleteMaster = (id) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        },
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/admin/master/delete/${id}`, config,
            (res) => {
                console.log(res);
                
                dispatch(updateStatus(ACTIONS.DELETE_MASTER_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.DELETE_MASTER_ERROR, err));
            });
    };
}

const setRoleMaster = (id) => {
    const ACCESS_TOKEN = JSON.parse(localStorage.getItem('USER_INFO')).user_info.access_token;
    const config = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ACCESS_TOKEN}`,
            accept: 'application/json',
            'X-CSRF-TOKEN': '',
        },
        body: JSON.stringify({
            id
        })
    };
    return (dispatch) => {
        callApi(`${host.serverTest}api/admin/master/role`, config,
            (res) => {
                console.log(res);
                
                dispatch(updateStatus(ACTIONS.SET_ROLE_MASTER_SUCCESS, res));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.SET_ROLE_MASTER_ERROR, err));
            });
    };
}


const convertListMaster = (data) => {
    return data && data.map((items, index) => {
        return [
            { text: items.name, id: items.id },
            { text: items.email },
            { text: items.phone },
            { text: items.role },
            {
                action: [
                    'Edit',
                    'Set Role',
                    'Delete'
                ]
            }
        ] 
            
        
    });
}

export { getListMaster, convertListMaster, deleteMaster, setRoleMaster, ACTIONS };
