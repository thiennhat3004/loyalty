
// core components/views for Admin layout
import ListMaster from './ListMaster/ListMaster';
// core components for RTL layout

const dashboardRoutes = [
    
    {
        path: '/list-master',
        name: 'List Master',
        component: ListMaster,
        layout: '/super-admin'
    }
];

export default dashboardRoutes;
