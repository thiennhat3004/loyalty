import React from 'react';
import { Switch, Route } from "react-router-dom";
import ListMaster from './ListMaster/ListMaster';
import ListContact from './List-contact/index';
import LeftMenu from './../../components/layout/nav/index';
import './styles.scss';
import Search from '../../components/search';
import { getProfile, logout } from './../../actions/common';
import { ACTIONS } from './../../actions/contants/contants';
import { connect } from 'react-redux';
import Loading from '../../components/loading';

interface Props {
    history: {
        push(url: string): any
    },
    location: any,
    ref: any,
    key: any,
    title: string,
    common: any,
    getProfile: any,
    logout: any
}

interface State {
    showLoading: boolean
}

class SuperAdmin extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            showLoading: false
        }
    }

    UNSAFE_componentWillMount() {
        this.props.getProfile();
    }

    UNSAFE_componentWillReceiveProps(nextProps: any) {
        
        if (nextProps && nextProps.status === ACTIONS.GET_PROFILE_SUCCESS) {
            if (nextProps.response.role === 1) {
                this.props.history.push('/admin');
            }
        }

        if (nextProps && nextProps.status === ACTIONS.LOGOUT_SUCCESS) {
            localStorage.removeItem('USER_INFO');
            this.props.history.push('/login');
        }
    }

    componentDidMount() {
    }

    logout() {
        this.setState({showLoading: true});
        this.props.logout();
    }

    render() {
        return (
            <div className='super-admin'>
                <LeftMenu
                    history={this.props.history}
                    location={this.props.location}
                />
                <main>
                    <div className='header-super-admin'>
                        <img alt='chuong' src='/images/ic_chuong_svg.svg' />
                        <img alt='logout' src='/images/ic_logout_svg.svg' onClick={() => this.logout()} />
                    </div>
                    <div className='content-super-admin'>
                        <div className='title'>
                            <h1>{'List master'}</h1>
                            <Search />
                        </div>
                        <div className='content'>
                            <Switch>
                                <Route path="/super-admin/list-master" exact component={ListMaster} />
                                <Route path="/super-admin/list-contact" component={ListContact} />
                                {/* <Route path="/player/:id/game-logs" component={GameLogs} />
                                <Route path="/player/:id/performance-by-year" component={PerformanceByYear} />
                                <Route path="/player/:id/ds-topic" component={TeamUserTopic} />
                                <Route path="/player/:id/user-topic" component={TeamUserTopic} />
                                <Route path="/player/:id/add-topic" component={PlayerAddTopic} />
                                <Route path="/player/:id/pitcher" component={PlayerPitcher} /> */}
                            </Switch>
                        </div>
                        
                    </div>
                    <div className='footer-super-admin'>
                        Design by TN
                    </div>
                </main>

                <Loading showLoading={this.state.showLoading} />
            </div>
        )
    }
}


const mapStateToProps = ({ common }: Props) => {
    return {
        response: common.response,
        status: common.status,
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        getProfile: () => {
            dispatch(getProfile())
        },
        logout: () => {
            dispatch(logout())
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(SuperAdmin);
