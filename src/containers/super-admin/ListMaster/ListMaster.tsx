import React, { Fragment } from 'react';
import './styles.scss';
import { connect } from 'react-redux';
import {
    ACTIONS,
    getListMaster,
    convertListMaster,
    deleteMaster,
    setRoleMaster
} from './../../../actions/super-admin/list-master';
import Modal from './../../../components/popup';
import Loading from '../../../components/loading';
import Table from './../../../components/Table/Table';

const tableListMaster = [
    [
        {
            text: 'thiennhat'
        },
        {
            text: 'thiennhat3004@gmail.com'
        },
        {
            text: '0935561693'
        },
        {
            text: '0'
        },
        {
            action: [
                'Edit',
                'Delete',
                'Set Role'
            ]
        }
    ],
    [
        {
            text: 'thiennhat2'
        },
        {
            text: 'thiennhat3004@gmail.com'
        },
        {
            text: '0935561693'
        },
        {
            text: '0'
        },
        {
            action: [
                'Edit',
                'Delete',
                'Set Role'
            ]
        }
    ],

]


interface Props {
    classes: any,
    getListMaster: any,
    listMaster: any,
    deleteMaster: any,
    setRoleMaster: any

    // modal
    showModal: any,
    titleModal: string,
    contentModal: string,
    isNotification: boolean,
    textAccept: string,
    onCloseModal: any,
    acceptAction: any
}

interface State {
    // listMaster: any
    showLoading: boolean,
    tableListMaster: Array<any>,
    modal: any,
    
}

class ListMaster extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            showLoading: false,
            tableListMaster: tableListMaster,

            // modal
            modal: {
                showModal: false,
                titleModal: '',
                contentModal: '',
                isNotification: true,
                textAccept: '',
                itemSelected: ''
            }
        }
    }

    componentDidMount() {
        this.setState({ showLoading: true });
        this.props.getListMaster();
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ showLoading: false });
        if (nextProps.response) {
            if (nextProps.status === ACTIONS.GET_LIST_MASTER_SUCCESS) {
                this.setState({
                    tableListMaster: convertListMaster(nextProps.response)
                })
            }
            if (nextProps.status === ACTIONS.DELETE_MASTER_SUCCESS) {
                console.log(nextProps.response);
                
                this.setState({
                    modal : {
                        showModal: true,
                        contentModal: nextProps.response.message,
                        isNotification: true,
                    }
                })
                this.props.getListMaster();
            }
            if (nextProps.status === ACTIONS.SET_ROLE_MASTER_SUCCESS) {
                this.props.getListMaster();
            }

        }


    }

    handleAction(item: any, i: any) {
        if (!i) {

        } else if (i === 1) {
            this.setState({
                showLoading: true
            })
            this.props.setRoleMaster(item[0].id);
        } else {
            console.log(item);
            this.setState({
                modal: {
                    showModal: true,
                    titleModal: 'Delete user!',
                    contentModal: 'Are you sure delete user ?',
                    isNotification: false,
                    itemSelected: item[0].id,
                    textAccept: 'Confirm'
                }
            })
            
        }
    }

    onCloseModal() {
        this.setState({
            modal : {
                showModal: false
            }
            
        })
    }

    acceptAction() {
        this.setState({showLoading: true});
        this.props.deleteMaster(this.state.modal.itemSelected);
    }

    render() {
        const { classes } = this.props;
        return (
            <Fragment>
                <div>
                    <Table
                        tableHead={['Name', 'Email', 'Phone', 'Role', 'Action']}
                        tableBody={this.state.tableListMaster}
                        handleAction={(value: any, i: any) => this.handleAction(value, i)}
                    />
                </div>

                <Modal
                    title={this.state.modal.titleModal}
                    showModal={this.state.modal.showModal}
                    content={this.state.modal.contentModal}
                    isNotification={this.state.modal.isNotification}
                    textAccept={this.state.modal.textAccept}
                    onCloseModal={() => this.onCloseModal()}
                    acceptAction={() => this.acceptAction()}
                />

                <Loading showLoading={this.state.showLoading} />
            </Fragment>
        )
    }


}

const mapStateToProps = ({ listMaster }: Props) => {
    return {
        response: listMaster.response,
        status: listMaster.status,
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        getListMaster: () => {
            dispatch(getListMaster())
        },
        deleteMaster: (id: number) => {
            dispatch(deleteMaster(id))
        },
        setRoleMaster: (id: number) => {
            dispatch(setRoleMaster(id))
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListMaster);
