import React, { Fragment } from "react";
import './styles.scss';
import { Link } from "react-router-dom";
import {
    connect
} from 'react-redux';
import FormErrors from './../../../common/services/validateServices';
import { sendForgotPassword } from './../../../actions/forgot-password';
import { ACTIONS } from './../../../actions/contants/contants';
import Loading from "../../../components/loading";
import Modal from "../../../components/popup";

interface ForgotPasswordState {
    email: string,
    formErrors: { email: string },
    emailValid: boolean,
    formValid: boolean,
    showLoading: boolean,
    modal: any,
}

interface ForgotPasswordProps {
    history: {
        push(url: string): void,
        goBack: any
    },
    forgotPassword: any,
    sendForgotPassword: any,
}

class ForgotPassword extends React.Component<ForgotPasswordProps, ForgotPasswordState> {

    constructor(props: ForgotPasswordProps) {
        super(props);
        this.state = {
            email: '',
            formErrors: { email: '' },
            emailValid: false,
            formValid: false,
            showLoading: false,

            modal : {
                showModal: false,
                titleModal: '',
                contentModal: '',
                isNotification: true,
                textAccept: ''
            }
        }
    }

    getFormResetPassword(e: any) {
        const value = e.target.value.toString();
        this.setState({
            email: value
        });
        this.validateField('email', value);
    }

    validateField(fieldName: any, value: any) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : 'is invalid';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.emailValid });
    }

    sendCodeEmail(e: any) {
        this.setState({showLoading: true});
        if (this.state.formValid) {
            const { email } = this.state;
            const data = {
                email
            }
            console.log(data);
            this.props.sendForgotPassword(data);
        }
    }

    componentWillReceiveProps(nextProps: any) {
        console.log(nextProps);
        if (nextProps.status === ACTIONS.SEND_FORGOT_PASSWORD_SUCCESS) {
            this.props.history.push(`/sent-email`);
            this.setState({
                showLoading: false
            })
        } else {
            this.setState({
                showLoading: false,
                modal : {
                    showModal: true,
                    titleModal: 'Error',
                    contentModal: nextProps.response,
                    isNotification: true
                }
            })
        }
    }

    handleBack() {
        this.props.history.goBack();
    }

    onCloseModal() {
        this.setState({
            modal : {
                showModal: false
            }
           
        })
    }

    render() {
        return (
            <Fragment>
                <div className="limiter forgot-password">
                    <div className="container-login100">
                        <div className="wrap-login100 p-t-20 p-b-20">
                            <form className="login100-form validate-form">
                                <span className="login100-form-title p-b-35">
                                    Forgot password
                              </span>

                                <div className='wrap-input100 validate-input m-t-0 m-b-35'>
                                    <input
                                        type='text'
                                        placeholder='Email'
                                        name="email"
                                        value={this.state.email}
                                        onChange={(e) => this.getFormResetPassword(e)}
                                    />
                                    <div className="notice">
                                        <FormErrors formErrors={this.state.formErrors} nameForm="forgotPassword" nameInput="email" />
                                    </div>
                                </div>

                                <div className="container-login100-form-btn">
                                    <button type='button' className='submit-login' disabled={!this.state.formValid} onClick={(e) => this.sendCodeEmail(e)}>
                                        Send code
                                    </button>
                                </div>
                                <ul className="login-more p-t-30">
                                    <li onClick={() => this.handleBack()} className='back-page'>
                                        <i className=" fas fa-arrow-left"></i>
                                        <span className="txt1">
                                            Back
                                        </span>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>

                <Loading showLoading={this.state.showLoading} />

                <Modal
                    title={this.state.modal.titleModal} 
                    showModal={ this.state.modal.showModal } 
                    content={this.state.modal.contentModal} 
                    isNotification={this.state.modal.isNotification}
                    acceptAction={() => {return false}}
                    textAccept={this.state.modal.textAccept}
                    onCloseModal={() => this.onCloseModal()}
                />
            </Fragment>
        )
    }
}


const mapStateToProps = ( {forgotPassword} : ForgotPasswordProps ) => {
    return {
        response: forgotPassword.response,
        status: forgotPassword.status
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        sendForgotPassword: (data: any) => {
            dispatch(sendForgotPassword(data));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
