import React, { Fragment, Dispatch } from "react";
import './styles.scss';
import { Link } from "react-router-dom";
import { login } from './../../../actions/login'; 
import { ACTIONS } from '../../../actions/contants/contants';
import {
    connect
} from 'react-redux';
import  FormErrors  from './../../../common/services/validateServices';
import Loading from './../../../components/loading/index';
import Modal from "../../../components/popup";

interface LoginState {
    [email: string] : any,
    formErrors: { email: string, password: string },
    emailValid: boolean,
    passwordValid: boolean,
    formValid: boolean,
    showLoading: boolean,
    modal: any,
    // isNotification: boolean,
    // textAccept: string
    // 
}

interface LoginProps {
    history : {
        push(url : string) : void
    },
    login : any,
    email : string,
    password : string
    random : any,
    response : any,

    showModal: any,
    titleModal: string,
    contentModal: string,
    isNotification: boolean,
    textAccept: string,
    onCloseModal: any

}

class Login extends React.Component<LoginProps, LoginState> {

    constructor(props : LoginProps) {
        super(props);
        this.state = {
            email : '',
            password : '',
            formErrors: { email: '', password: '' },
            emailValid: false,
            passwordValid: false,
            formValid: false,
            showLoading: false,
            // modal
            modal : {
                showModal: false,
                titleModal: '',
                contentModal: '',
                isNotification: true,
                textAccept: ''
            }
            // showModal: false,
            //     titleModal: '',
            //     contentModal: '',
            //     isNotification: true,
            //     textAccept: ''
            
        }
    }

    getFormLogin(e : any) {
        const name  = e.target.name.toString();
        const value = e.target.value.toString();
        this.setState({
            [name] : value
        });
        this.validateField(name, value);
    }

    validateField(fieldName: any, value: any) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : 'is invalid';
                break;
            case 'password':
                passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? '' : 'is too short';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            passwordValid: passwordValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.emailValid && this.state.passwordValid });
    }

    submitLogin(e : any) {
        this.setState({showLoading:true});
        if (this.state.formValid) {
            const { email, password } = this.state;
            const address_device = localStorage.getItem('address_device');
            const data = {
                email : email,
                password : password,
                address_device : address_device
            }
            this.props.login(data);
        }
    }

    componentWillReceiveProps(nextProps: any) {
        console.log('logout ra login');
        
        this.setState({showLoading: false});
        if (nextProps) {
            if (nextProps.status === ACTIONS.LOGIN_SUCCESS) {
                
                console.log(nextProps.response);
                if (nextProps.response.user_info) {
                    const address_device = localStorage.getItem('address_device');
                    if (!address_device) {
                        let address = {
                            token_address : nextProps.response.user_info.access_token
                        }
                        localStorage.setItem('address_device', JSON.stringify(address));
                    }
                    localStorage.setItem('USER_INFO', JSON.stringify(nextProps.response));
                    switch (nextProps.response.user_info.role) {
                        case 2: 
                            this.props.history.push(`/super-admin`)
                            break;
                        case 1: 
                            this.props.history.push(`/admin`)
                            break;
                        default:
                            this.props.history.push(`/wait-accept`)
                    }
                } else {
                    this.setState({
                        modal : {
                            showModal: true,
                            titleModal: '',
                            contentModal: nextProps.response.message,
                            isNotification: true
                        }
                        
                    })
                }
            }
        }
    }

    onCloseModal() {
        this.setState({
            modal : {
                showModal: false
            }
        })
    }

    render() {
        return (
            <Fragment>
                <div className="limiter">
                    <div className="container-login100">
                        <div className="wrap-login100 p-t-20 p-b-20">
                            <form className="login100-form validate-form" >
                                <span className="login100-form-title p-b-35">
                                    Welcome
                              </span>
                                
                                <div className='wrap-input100 validate-input m-t-0 m-b-35'>
                                    <input 
                                        type='text' 
                                        name="email" 
                                        value={ this.state.username } 
                                        onChange={(e) => this.getFormLogin(e)}
                                        placeholder='Enter email'
                                    />
                                    <div className="notice">
                                        <FormErrors formErrors={this.state.formErrors} nameForm="login" nameInput="email" />
                                    </div>
                                </div>
                                 <div className='wrap-input100 validate-input m-b-50'>
                                 
                                        <input 
                                        type='password' 
                                        placeholder='Enter password'
                                        name="password" 
                                        value={ this.state.pass } 
                                        onChange={(e) => this.getFormLogin(e)}
                                        />
                                        <div className="notice">
                                            <FormErrors formErrors={this.state.formErrors} nameForm="login" nameInput="password" />
                                        </div>
                                 </div>
        
                                <div className="container-login100-form-btn">
                                    <button type='button' disabled={!this.state.formValid} className='submit-login' onClick={(e) => this.submitLogin(e)}>
                                        Login
                                    </button>
                                </div>
                                <ul className="login-more p-t-30">
                                    <li className="m-b-8">
                                        <span className="txt1">
                                            Forgot <Link to="forgot-password" className="txt2"> Email / Password?</Link>
                                        </span>
                                    </li>
                                    <li>
                                        <span className="txt1">
                                            Don’t have an account ? <Link to='signup' className="txt2"> Sign up </Link>
                                        </span>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div> 
            
                <Loading showLoading={this.state.showLoading} />

                <Modal 
                    title={this.state.modal.titleModal} 
                    showModal={ this.state.modal.showModal } 
                    content={this.state.modal.contentModal} 
                    isNotification={this.state.modal.isNotification}
                    acceptAction={() => {return false}}
                    textAccept={this.state.modal.textAccept}
                    onCloseModal={() => this.onCloseModal()}
                />
            </Fragment>
        )
    }
}

const mapStateToProps = ( {login} : LoginProps ) => {
    return {
        response : login.response,
        status : login.status,
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        login: (data: Object) => {
            dispatch(login(data))
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);