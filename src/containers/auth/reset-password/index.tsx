import React, { Fragment, Dispatch } from "react";
import './styles.scss';
import { Link } from "react-router-dom";
import {
    connect
} from 'react-redux';
import FormErrors from './../../../common/services/validateServices';
import queryString from 'query-string';
import { resetPassword } from './../../../actions/reset-password';
import { ACTIONS } from './../../../actions/contants/contants';
import Loading from "../../../components/loading";

interface ResetPasswordState {
    [password: string]: any,
    passwordValid: boolean,
    rePasswordValid: boolean,
    password: string,
    rePassword: string,
    formErrors: { password: string, rePassword: string },
    formValid: boolean
}

interface ResetPasswordProps {
    history: {
        push(url: string): void
    },
    location: any,
    resetPassword: any,
    showLoading: boolean

}

class ResetPassword extends React.Component<ResetPasswordProps, ResetPasswordState> {

    constructor(props: ResetPasswordProps) {
        super(props);
        this.state = {
            password: '',
            rePassword: '',
            passwordValid: false,
            rePasswordValid: false,
            formErrors: { password: '', rePassword: '' },
            formValid: false,
            showLoading: false
        }
    }

    getFormResetPassword(e: any) {
        const name = e.target.name.toString();
        const value = e.target.value.toString();
        this.setState({
            [name]: value
        });
        this.validateField(name, value);
    }

    validateField(fieldName: any, value: any) {
        let fieldValidationErrors = this.state.formErrors;
        let { passwordValid, rePasswordValid } = this.state;

        switch (fieldName) {
            case 'password':
                passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? '' : 'is too short';

                if (this.state.rePasswordValid) {
                    passwordValid = value == this.state.rePassword;
                    fieldValidationErrors.password = passwordValid ? '' : 'not match';
                }

                break;
            case 'rePassword':
                rePasswordValid = value == this.state.password;
                fieldValidationErrors.rePassword = rePasswordValid ? '' : ' not match';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            passwordValid: passwordValid,
            rePasswordValid: rePasswordValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.passwordValid && this.state.rePasswordValid });
    }




    submitResetPassword(e: any) {
        this.setState({showLoading: true});
        if (this.state.formValid) {
            const search = this.props.location.search;
            const param = queryString.parse(search);
            const { password } = this.state;
            const body = {
                password,
                email: param.email,
                token: param.tk
            }
            console.log(param);
            this.props.resetPassword(body);
        }

    }

    componentWillReceiveProps(nextProps: any) {
        if (nextProps.status === ACTIONS.RESET_PASSWORD_SUCCESS) {
            if (nextProps.response.user_info) {
                this.setState({showLoading: false});
                localStorage.setItem('USER_INFO', JSON.stringify(nextProps.response));
                switch (nextProps.response.user_info.role) {
                    case 2:
                        this.props.history.push(`/super-admin`)
                        break;
                    default:
                        this.props.history.push(`/admin`)
                }
            }

        }
    }

    render() {
        return (
            <Fragment>
                <div className="limiter">
                    <div className="container-login100">
                        <div className="wrap-login100 p-t-20 p-b-20">
                            <form className="login100-form validate-form">
                                <span className="login100-form-title p-b-35">
                                    Reset password
                              </span>

                                <div className='wrap-input100 validate-input m-t-0 m-b-35'>
                                    <input
                                        type='password'
                                        name="password"
                                        placeholder='Enter new password'
                                        onChange={(e) => this.getFormResetPassword(e)}
                                        value={this.state.password}
                                    />
                                    <div className="notice">
                                        <FormErrors formErrors={this.state.formErrors} nameForm="resetPassword" nameInput="password" />
                                    </div>
                                </div>
                                <div className='wrap-input100 validate-input m-b-50'>

                                    <input
                                        type='password'
                                        placeholder='Enter new re-password '
                                        name="rePassword"
                                        onChange={(e) => this.getFormResetPassword(e)}
                                        value={this.state.rePassword}
                                    />
                                    <div className="notice">
                                        <FormErrors formErrors={this.state.formErrors} nameForm="resetPassword" nameInput="rePassword" />
                                    </div>
                                </div>

                                <div className="container-login100-form-btn">
                                    <button type='button' className='submit-login' onClick={(e) => this.submitResetPassword(e)}>
                                        Reset
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <Loading showLoading={this.state.showLoading} />
            </Fragment>
        )
    }
}




const mapStateToProps = ({ resetPassword }: ResetPasswordProps) => {
    return {
        status: resetPassword.status,
        error: resetPassword.error,
        response: resetPassword.response
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        resetPassword: (data: any) => {
            dispatch(resetPassword(data))
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);