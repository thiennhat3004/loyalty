import React, { Fragment } from "react";
import './styles.scss';


interface Props {
    history : {
        push(url : string) : void,
        goBack: any,
    },
    
    signup: any
}

export function WaitAccept(props: Props) {

    const backPage = () => {
        props.history.goBack();
    }
    return(
        <div className='wait-accept'>
            <div className='notification-signup'>
            Thank you for registering. Please wait for account activation from admin !<br/> <br/>
            <button onClick={ backPage}>Back</button>
            </div>
            
        </div>
    )
}