import React, { Fragment } from "react";
import './styles.scss';
import { Link } from "react-router-dom";
import  FormErrors  from './../../../common/services/validateServices';
import {
    connect
} from 'react-redux';
import { ACTIONS, signup } from './../../../actions/signup';
import Loading from "../../../components/loading";

interface SignupState {
    [key : string] : any,
    formErrors: { email: string, password: string, confirmPassword: string },
    emailValid: boolean,
    passwordValid: boolean,
    confirmPasswordValid: boolean,
    formValid: boolean,
    showLoading: boolean
}

interface SignupProps {
    history : {
        push(url : string) : void
    },
    signup: any
}

class Signup extends React.Component<SignupProps, SignupState> {

    constructor(props : SignupProps) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: '',
            confirmPassword: '',
            formErrors: { email: '', password: '', confirmPassword: '' },
            emailValid: false,
            passwordValid: false,
            confirmPasswordValid: false,
            formValid: false,
            showLoading: false
        }
    }

    getInputRegister(e: any) {
        let name = e.target.name.toString();
        let value = e.target.value.toString();
        this.setState({
            [name] : value
        });
        this.validateField(name, value);
    }

    validateField(fieldName: any, value: any) {
        let fieldValidationErrors = this.state.formErrors;
        let { emailValid, passwordValid, confirmPasswordValid } = this.state;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : 'is invalid';
                break;
            case 'password':
                passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? '' : 'is too short';
                
                if (this.state.confirmPasswordValid) {
                    passwordValid = value == this.state.confirmPassword;
                    fieldValidationErrors.password = passwordValid ? '' : 'not match';
                }
                
                break;
            case 'confirmPassword':
                confirmPasswordValid = value == this.state.password;
                fieldValidationErrors.confirmPassword = confirmPasswordValid ? '' : ' not match';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            passwordValid: passwordValid,
            confirmPasswordValid: confirmPasswordValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.emailValid && this.state.passwordValid });
    }

    sendRegister(e: any) {
        this.setState({showLoading:true});
        if(this.state.formValid) {
            let { username, email, password } = this.state;
            const body = {
                name : username,
                email,
                password
            };
            console.log(body);
            this.props.signup(body);
        }
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({showLoading:false})
        console.log(nextProps);
        if (nextProps.status === ACTIONS.SIGNUP_SUCCESS) {

            this.props.history.push(`/wait-accept`);
        }
        
    }

    render() {
        return (
            <Fragment>
                <div className="limiter signup">
                    <div className="container-login100">
                        <div className="wrap-login100 p-t-20 p-b-20">
                            <form className="login100-form validate-form">
                                <span className="login100-form-title p-b-35">
                                    Register
                              </span>
                              <div className="wrap-input100 validate-input m-t-0 m-b-35" data-validate="Enter username">
                                    <span className="focus-input100" data-placeholder="Email" >Name</span>
                                    <input 
                                        placeholder='Full name' 
                                        className="input100" 
                                        type="text" name="username" 
                                        value={ this.state.username } 
                                        onChange={(e) => this.getInputRegister(e)}  />
                                    
                                </div>
                                <div className="wrap-input100 validate-input m-t-0 m-b-35" data-validate="Enter email">
                                    <span className="focus-input100" data-placeholder="Email" >Email</span>
                                    <input 
                                        placeholder='Enter email'
                                        className="input100" 
                                        type="text" name="email" 
                                        value={ this.state.email } 
                                        onChange={(e) => this.getInputRegister(e)} />
                                    <div className="notice">
                                        <FormErrors formErrors={this.state.formErrors} nameForm="register" nameInput="email" />
                                    </div>    
                                </div>
                                <div className="wrap-input100 validate-input m-b-50" data-validate="Enter password">
                                    <span className="focus-input100" data-placeholder="Password" >Password</span>
                                    <input 
                                        placeholder='Enter password' 
                                        className="input100" 
                                        type="password" 
                                        name="password" 
                                        value={ this.state.password } 
                                        onChange={(e) => this.getInputRegister(e)} />
                                    <div className="notice">
                                        <FormErrors formErrors={this.state.formErrors} nameForm="register" nameInput="password" />
                                    </div>    
                                </div>
                                <div className="wrap-input100 validate-input m-b-50" data-validate="Enter password">
                                    <span className="focus-input100" data-placeholder="Password" >Re-password</span>
                                    <input 
                                        placeholder='Enter re-password' 
                                        className="input100" 
                                        type="password" name="confirmPassword" 
                                        value={ this.state.confirmPassword } 
                                        onChange={(e) => this.getInputRegister(e)} />
                                    <div className="notice">
                                        <FormErrors formErrors={this.state.formErrors} nameForm="register" nameInput="confirmPassword" />
                                    </div>    
                                </div>
                                <div className="container-login100-form-btn">
                                    <button type='button' disabled={!this.state.formValid} onClick={(e) => this.sendRegister(e)} className="login100-form-btn submit-register" >
                                        Register
                                    </button>
                                </div>
                                <ul className="login-more p-t-30">
                                    <li>
                                        <span className="txt1">
                                            Do you have account? <Link to='login' className="txt2"> Login </Link>
                                        </span>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div> 
                <Loading showLoading={this.state.showLoading} />
            </Fragment>
        )
    }
}


const mapStateToProps = ( {signup} : SignupProps ) => {
    return {
        response : signup.response,
        status : signup.status,
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        signup: (data: Object) => {
            dispatch(signup(data))
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
