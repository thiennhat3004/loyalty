import React, { Fragment } from "react";
import './styles.scss';


interface Props {
    history: {
        push(url: string): void,
        goBack: any,
    },

}

export function SentEmail(props: Props) {

    const backPage = () => {
        props.history.goBack();
    }
    return (
        <div className='wait-accept'>
            <div className='notification-signup'>
                <p>A reset link has been sent to your email address.</p>
                <button onClick={backPage}>Back</button>
            </div>
            
        </div>
    )
}