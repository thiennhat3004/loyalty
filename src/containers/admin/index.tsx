import React, { Fragment }  from "react";
import './styles.scss';
import { connect } from 'react-redux';
import { Switch, Route, Router, Link } from "react-router-dom";
import Dashboard from "./dashboard/index";
import AddLoyalty from "./add-loyalty/index";
import Members from "./members";
import ManageMember from "./members/manage-member";
import { ACTIONS } from './../../actions/contants/contants';
import { logout } from './../../actions/common';
import Loading from "../../components/loading";

interface MyProps {
    history : {
        push(url : string) : void
    },
    logout: any,
    common: any
}

interface MyState  {
    showLoading: boolean,
}

class Admin extends React.Component<MyProps, MyState> {
    
    constructor(props: MyProps) {
        super(props);
        this.state = {
            showLoading: false
        }
    }

    logout() {
        this.setState({showLoading: true});
        this.props.logout();
    }

    UNSAFE_componentWillReceiveProps(nextProps: any) {
        if (nextProps.response) {
            if (!nextProps.response.error) {
                localStorage.removeItem('USER_INFO');
                this.props.history.push(`/login`);
            }
        }
    }
   

    render() {
        return(
            <Fragment>
                <div className='wrap-admin'>
                    <div className='bar-top'>
                        <img src='https://myloyalty.me/media/various/myloyalty-logo.png' alt='logo' />
                        <ul>
                            <li>
                                <i className="fas fa-bell"></i>
                            </li>
                            <li>
                                <div>
                                    <span>Thien nhat</span>
                                    <span>Adminstrator</span>
                                </div>
                                <div className='logout' onClick={() => this.logout()}>
                                    Logout
                                </div>
                                <div className='wrap-logo'>
                                    <img alt='profile' src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1200px-Circle-icons-profile.svg.png' />
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className='title-option-admin'>
                        <ul>
                            <Link to='/admin'>
                                <li >
                                    <i className="fas fa-home"></i>
                                    Home
                                </li>
                            </Link>
                            
                            <Link to='/'>
                                <li>
                                    <i className="far fa-paper-plane"></i>
                                    Send Message
                                </li>
                            </Link>
                            <Link to='/'>
                                <li>
                                    <i className="fas fa-id-card"></i>
                                    Contact
                                </li>
                            </Link>
                        </ul>
                    </div>
                    <div className='content-option-admin'>
                            <Switch>
                                <Route path='/admin' exact component={Dashboard} />
                                <Route path='/admin/add-program' exact component={AddLoyalty} />
                                <Route path='/admin/edit-program' exact component={AddLoyalty} />
                                <Route path='/admin/members' exact component={Members} />
                                <Route path='/admin/members/manage' exact component={ManageMember} />
                            </Switch>
                    </div>
                </div>
                
                
                <Loading showLoading={this.state.showLoading} />
            </Fragment>
            
        )
    }
}



const mapStateToProps = ({ common }: MyProps) => {
    return {
        status: common.status,
        error: common.error,
        response: common.response
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        logout: () => {
            dispatch(logout())
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
