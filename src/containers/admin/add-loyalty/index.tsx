import React, { Component, Fragment } from "react";
import './styles.scss';
import { EMLINK } from "constants";
import { AddProgram } from "./add-program";
import AddPackage from './add-package/index';
import { ACTIONS } from './../../../actions/contants/contants';
import { addProgram, getListScreenName, getListScreenDevice } from './../../../actions/add-loyalty';
import { connect } from 'react-redux';
import Loading from "../../../components/loading";


const screenDefault = {
    screen_name: 'ggg',
    main_heading: 'FREE 16oz',
    sub_heading: 'Recive 2 FREE oz and 1 point when you join now.It"s free to get started and only takes a second!',
    bg_color: '#eab7c0',
    logo_width: 100,
    message: 'g',
    logo_url: '/images/logo_menchies.png',
    text_color: '#ffffff',
    primary_color: '#17176b',
    btn_color: '#1a96bb',
    btn_text_color: '#ffffff',
    icon_color: '#ffffff',
}

const arrItemLayout: any = [
    // {
    //     idLayout: 1,
    //     device_id: 10,
    //     screen_name: 'ggg',
    //     main_heading: 'FREE 16oz',
    //     sub_heading: 'Recive 2 FREE oz and 1 point when you join now.It"s free to get started and only takes a second!',
    //     bg_color: '#eab7c0',
    //     // widthLogo: 100,
    //     message: '',
    //     logo_url: '/images/logo_menchies.png',
    //     text_color: '#ffffff',
    //     primary_color: '#17176b',
    //     btn_color: '#1a96bb',
    //     btn_text_color: '#ffffff',
    //     icon_color: '#ffffff',
    // },
    // {
    //     idLayout: 2,
    //     device_id: 11,
    //     screen_name: 'hhhhh',
    //     main_heading: 'FREE 16oz',
    //     sub_heading: 'Recive 2 FREE oz and 1 point when you join now.It"s free to get started and only takes a second!',
    //     bg_color: '#eab7c0',
    //     // widthLogo: 100,
    //     message: '',
    //     logo_url: '/images/logo_menchies.png',
    //     text_color: '#ffffff',
    //     primary_color: '#17176b',
    //     btn_color: '#1a96bb',
    //     btn_text_color: '#ffffff',
    //     icon_color: '#ffffff',
    // }
]

interface AddLoyaltyProps {
    history: {
        push(url: string): any
    },
    location: any,
    isPageAdd: boolean,
    addLoyalty: any,
    addProgram: any,
    addPackage: any,
    addScreen: any,
    getListScreenName: any,
    getListScreenDevice: any
}

interface AddLoyaltyState {
    [key: string]: any,
    listItemReward: Array<Object>,
    showLoading: boolean

}

class AddLoyalty extends React.Component<AddLoyaltyProps, AddLoyaltyState> {

    constructor(props: AddLoyaltyProps) {
        super(props);
        this.state = {
            showLoading :false,
            isPageAdd: true,
            showScreenCheckin: false,
            program : {
                nameProgram: '',
                desProgram: '',
                howReward: 1,
                timeCheckin: 0,
                businessName: ''
            },
            listScreenName: [],
            listItemReward: [],
            listLayoutScreen: [],

            idLayout: 1,
            deviceId: 0,
            screenName: 'lll',
            mainHeading: 'FREE 1234',
            mainSubHeading: 'Recive 2 FREE oz and 1 point when you join now.It"s free to get started and only takes a second!',
            backgroundColor: '#eab7c0',
            widthLogo: 150,
            urlLogo: '/image/logo_menchies.png',
            textColor: '#ffffff',
            primaryColor: '#17176b',
            buttonColor: '#1a96bb',
            buttonTextColor: '#ffffff',
            iconColor: '#ffffff',
        }
    }

    UNSAFE_componentWillMount() {
        const url = this.props.location.pathname;
        if (url.includes('add-program')) {
            this.setState({
                isPageAdd: true
            })
        } else {
            this.setState({
                isPageAdd: false
            })
        }
    }

    componentDidMount() {
        this.setState({
            listLayoutScreen: arrItemLayout
        })

        // get list screen name
        this.props.getListScreenName();
        if (!this.state.isPageAdd) {
            this.props.getListScreenDevice();
        }
    }



    changeColorLayout(e: any,i: number,id: number) {
        const name = e.target.name;
        const value = e.target.value;
        
        this.setState({
            [name]: value
        }, () => {
            let objNew: any;
            const { 
                deviceId,
                screenName, 
                mainHeading, 
                mainSubHeading, 
                backgroundColor, 
                widthLogo,
                urlLogo, 
                textColor,
                primaryColor,
                buttonColor,
                buttonTextColor,
                iconColor
            } = this.state;
            if (id) {
                objNew = {
                    id: id,
                    device_id: deviceId,
                    main_heading: mainHeading,
                    sub_heading: mainSubHeading,
                    message: 'g',
                    logo_width: widthLogo,
                    logo_url: urlLogo,
                    bg_color: backgroundColor,
                    text_color: textColor,
                    primary_color: primaryColor,
                    btn_color: buttonColor,
                    btn_text_color: buttonTextColor,
                    icon_color: iconColor
                };
            } else {
                objNew = {
                    device_id: deviceId,
                    main_heading: mainHeading,
                    sub_heading: mainSubHeading,
                    message: 'g',
                    logo_url: urlLogo,
                    logo_width: widthLogo,
                    bg_color: backgroundColor,
                    text_color: textColor,
                    primary_color: primaryColor,
                    btn_color: buttonColor,
                    btn_text_color: buttonTextColor,
                    icon_color: iconColor
                };
            }

            console.log(value);
            

            this.state.listScreenName.forEach((element: any) => {
                if (deviceId == element.id) {
                    objNew.screen_name = element.name;
                }
            });
            
            this.state.listLayoutScreen.splice(i, 1, objNew);
            this.setState({
                listLayoutScreen: this.state.listLayoutScreen
            })
        })
        

    }

    showContentScreenOption(e: any, item: any) {
        const elem = e.target.parentNode.parentNode.nextSibling;
        const contentScreenOption = document.querySelectorAll('.content-screen-option');
        const checkMh = elem.classList.contains('maxheight-auto-screen');
        this.setState({

            idLayout: item.idLayout
        });
        if (checkMh) {
            elem.classList.remove('maxheight-auto-screen');
            this.setState({
                showScreenCheckin: false
            })
        } else {
            contentScreenOption.forEach(val => {
                val.classList.remove('maxheight-auto-screen');
            })
            elem.classList.add('maxheight-auto-screen');
            console.log(item);
            
            this.setState({
                showScreenCheckin: true,

                screenName: item.screen_name,
                mainHeading: item.main_heading,
                mainSubHeading: item.sub_heading,
                backgroundColor: item.bg_color,
                message: 'g',
                widthLogo: item.logo_width,
                urlLogo: item.logo_url,
                textColor: item.text_color,
                primaryColor: item.primary_color,
                buttonColor: item.btn_color,
                buttonTextColor: item.btn_text_color,
                iconColor: item.icon_color,
            })
        }
    }

    copyItemScreen(item: any) {
        const arrNew = this.state.listLayoutScreen;
        let objNew = Object.create(item);
        console.log(item);
        
        objNew = {

            screen_name: item.screen_name,
            device_id: item.device_id,
            main_heading: item.main_heading,
            sub_heading: item.sub_heading,
            bg_color: item.bg_color,
            message: 'g',
            logo_width: item.logo_width,
            logo_url: item.logo_url,
            text_color: item.text_color,
            primary_color: item.primary_color,
            btn_color: item.btn_color,
            btn_text_color: item.btn_text_color,
            icon_color: item.icon_color,
        }
        arrNew.push(objNew);

        this.setState({
            listLayoutScreen: arrNew
        })
    }

    deleteItemScreen(index: number) {
        console.log(index);

        this.state.listLayoutScreen.splice(index, 1);
        this.setState({
            listLayoutScreen: this.state.listLayoutScreen,
            showScreenCheckin: false
        })
    }

    addItemScreenEmpty() {
        const objEmpty = {
            device_id: this.state.deviceId,
            main_heading: screenDefault.main_heading,
            sub_heading: screenDefault.sub_heading,
            message: 'g',
            logo_url: screenDefault.logo_url,
            logo_width: screenDefault.logo_width,
            bg_color: screenDefault.bg_color,
            text_color: screenDefault.text_color,
            primary_color: screenDefault.primary_color,
            btn_color: screenDefault.btn_color,
            btn_text_color: screenDefault.btn_text_color,
            icon_color: screenDefault.icon_color
        }
        const arrNew = this.state.listLayoutScreen;
        arrNew.push(objEmpty);
        this.setState({
            listLayoutScreen: arrNew
        })
    }



    getValueProgram(data: any) {
        
        const { nameProgram, desProgram, howReward, timeCheckin, businessName } = data;
        this.setState({
            program : {
                nameProgram,
                desProgram,
                howReward,
                timeCheckin,
                businessName
            }
        });
    }

    
    getValuePackage(data: any) {
        this.setState({
            listItemReward: data
        })
    }

    submitLoyalty() {
        this.setState({showLoading: true});
        if(this.state.isPageAdd) {
            const { nameProgram, desProgram, howReward, timeCheckin, businessName  } = this.state.program;
            let bodyProgram = {
                program_name: nameProgram,
                description: desProgram,
                how_reward: howReward,
                time_checkin: timeCheckin,
                business_name: businessName,
                package: this.state.listItemReward,
                screens: this.state.listLayoutScreen
            };

            console.log(bodyProgram);
            
            this.props.addProgram(bodyProgram);
        } else {
            // edit
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps: any) {
        if (nextProps) {
            if (nextProps.status === ACTIONS.ADD_PROGRAM_SUCCESS) {
                this.props.history.push('/admin');
                
            }

            if (nextProps.status === ACTIONS.GET_LIST_SCREEN_NAME_SUCCESS) {
                console.log(nextProps.response);
                
                this.setState({
                    listScreenName: nextProps.response
                },() => {
                    this.setState({
                        deviceId: nextProps.response[0].id
                    })
                })
                
            }

            if (nextProps.status === ACTIONS.GET_LIST_SCREEN_DEVICE_SUCCESS) {
                console.log(nextProps.response);
                this.setState({
                    listLayoutScreen: nextProps.response
                })
            }
        }
    }

    render() {
        const {
            deviceId,
            screenName,
            mainHeading,
            mainSubHeading,
            backgroundColor,
            widthLogo,
            urlLogo,
            textColor,
            primaryColor,
            buttonColor,
            buttonTextColor,
            iconColor
        } = this.state;

        return (
            <Fragment>
                <div className='wrap-add-loyalty'>
                    <h1>Create a loyalty reward program</h1>
                    <p>Build your program by  following the steps outlined below</p>
                    <div className='add-loyalty'>

                        <div className='option-add-loyalty'>
                            <div className='wrap-collapse-loyalty'>
                                {/* wrap collapse */}
                                <div className="wrap-collabsible">

                                    {/* item collapse */}
                                    <input id="collapsibleOne" className="toggle" type="checkbox" />
                                    <label htmlFor="collapsibleOne" className="lbl-toggle">1.Program Settings</label>
                                    <div className="collapsible-content">
                                        <AddProgram valueProgram={(value: any) => this.getValueProgram(value)} />
                                    </div>
                                    {/* item collapse */}

                                    {/* item collapse */}
                                    <input id="collapsibleTwo" className="toggle" type="checkbox" />
                                    <label htmlFor="collapsibleTwo" className="lbl-toggle">2.Reward Offers</label>
                                    <div className="collapsible-content">
                                        <AddPackage location={this.props.location} isPageAdd={this.state.isPageAdd} getValuePackage={(data: any)=>this.getValuePackage(data)} />
                                    </div>
                                    {/* item collapse */}

                                    {/* item collapse */}
                                    <input id="collapsibleThree" className="toggle" type="checkbox" />
                                    <label htmlFor="collapsibleThree" className="lbl-toggle">3.Check-In Screens</label>
                                    <div className="collapsible-content">
                                        <div className="content-inner">
                                            <div className='wrap-option-screen'>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam feugiat lacus et quam commodo finibus. Suspendisse gravida consequat enim sed hendrerit. Nulla vitae purus egestas</p>
                                                <br></br>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam feugiat lacus et quam commodo finibus. Suspendisse gravida consequat enim sed hendrerit. Nulla vitae purus egestas</p>
                                                {/* item option screen */}

                                                {
                                                    this.state.listLayoutScreen.length && this.state.listLayoutScreen.map((val: any, i: number) => {
                                                        return (
                                                            <div className='screen-option' key={i} >
                                                                <div className='title-screen-option'>
                                                                    <div>
                                                                        <i className="fas fa-tablet-alt"></i>
                                                                        <h4>
                                                                            {val.screen_name}
                                                                        </h4>
                                                                    </div>

                                                                    <ul>
                                                                        <li className="fas fa-cog" onClick={(e) => this.showContentScreenOption(e, val)}></li>
                                                                        <li className="far fa-copy" onClick={() => this.copyItemScreen(val)}  ></li>
                                                                        <li className="fas fa-trash-alt" onClick={() => this.deleteItemScreen(i)} ></li>
                                                                    </ul>
                                                                </div>
                                                                <div className='content-screen-option'>
                                                                    <div className='input-option-program'>
                                                                        <label>Screen Name (customer will not see)</label>
                                                                        <select className='form-text' name='deviceId'  onChange={(e) => this.changeColorLayout(e, i, val.id)} >
                                                                            {
                                                                                this.state.listScreenName.map((v: any, index: number) => {
                                                                                    if (v.name) {
                                                                                        return <option key={index} value={v.id}>{v.name}</option>
                                                                                    } else {
                                                                                        return null;
                                                                                    }
                                                                                })
                                                                            }
                                                                        </select>
                                                                    </div>

                                                                    <div className='input-option-program'>
                                                                        <label>Subscribe new people to a textword?</label>
                                                                        <select className='form-text' name='subscribe-people'>
                                                                            <option>No</option>
                                                                        </select>
                                                                    </div>

                                                                    <div className='input-option-program'>
                                                                        <label>Main Heading</label>
                                                                        <input type='text' className='form-text' name='mainHeading' value={mainHeading} onChange={(e) => this.changeColorLayout(e, i, val.id)} />
                                                                    </div>

                                                                    <div className='input-option-program'>
                                                                        <label>Main Sub-Heading</label>
                                                                        <input type='text' className='form-text' name='mainSubHeading' value={mainSubHeading} onChange={(e) => this.changeColorLayout(e, i, val.id)} />
                                                                    </div>

                                                                    <div className='input-option-program'>
                                                                        <label>Fine Print</label>
                                                                        <textarea className='form-text' name='finePrint' >

                                                                        </textarea>
                                                                    </div>

                                                                    <div className='find-url'>
                                                                        <div className='input-option-program'>
                                                                            <label>Logo URL</label>
                                                                            <input type='text' className='form-text' name='urlLogo' value={urlLogo} onChange={(e) => this.changeColorLayout(e, i, val.id)} />
                                                                        </div>
                                                                        <button>Find</button>
                                                                    </div>

                                                                    <div>
                                                                        <label>Logo Size</label>
                                                                        <input type='range' min='100' max='250' name='widthLogo' value={widthLogo} onChange={(e) => this.changeColorLayout(e, i, val.id)} />
                                                                    </div>

                                                                    <div className='input-option-program'>
                                                                        <label>Background Color</label>
                                                                        <input type='color' className='form-text' name='backgroundColor' value={backgroundColor} onChange={(e) => this.changeColorLayout(e, i, val.id)} />
                                                                    </div>

                                                                    <div className='input-option-program'>
                                                                        <label>Text Color</label>
                                                                        <input type='color' className='form-text' name='textColor' value={textColor} onChange={(e) => this.changeColorLayout(e, i, val.id)} />
                                                                    </div>

                                                                    <div className='input-option-program'>
                                                                        <label>Primary Color</label>
                                                                        <input type='color' className='form-text' name='primaryColor' value={primaryColor} onChange={(e) => this.changeColorLayout(e, i, val.id)} />
                                                                    </div>

                                                                    <div className='input-option-program'>
                                                                        <label>Button Color</label>
                                                                        <input type='color' className='form-text' name='buttonColor' value={buttonColor} onChange={(e) => this.changeColorLayout(e, i, val.id)} />
                                                                    </div>

                                                                    <div className='input-option-program'>
                                                                        <label>Button Text Color</label>
                                                                        <input type='color' className='form-text' name='buttonTextColor' value={buttonTextColor} onChange={(e) => this.changeColorLayout(e, i, val.id)} />
                                                                    </div>

                                                                    <div className='input-option-program'>
                                                                        <label>Icon Color</label>
                                                                        <input type='color' className='form-text' name='iconColor' value={iconColor} onChange={(e) => this.changeColorLayout(e, i, val.id)} />
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }

                                                {/* item option screen */}
                                                <div className='add-item-screen' onClick={() => this.addItemScreenEmpty()}>
                                                    <i className="fas fa-plus-circle"></i>
                                                    <span >Add Check-In Screen</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/* item collapse */}

                                    {/* item collapse */}
                                    <input id="collapsibleFour" className="toggle" type="checkbox" />
                                    <label htmlFor="collapsibleFour" className="lbl-toggle">4.SMS Updates</label>
                                    <div className="collapsible-content">
                                        <div className="content-inner">
                                            <p>
                                                noi dung 4
                                        </p>
                                        </div>
                                    </div>
                                    {/* item collapse */}

                                </div>
                                {/* wrap collapse */}
                            </div>
                            <div className='group-btn-loyalty'>
                                <button>Cancel</button>
                                <button onClick={() => this.submitLoyalty()}>Save Program</button>
                            </div>
                        </div>

                        <div className='screen-add-loyalty'>
                            <h4>Sample Check-In Screen</h4>
                            <div className='screen-loyalty'>
                                {
                                    this.state.showScreenCheckin
                                        ? (
                                            <div className='screen-checkin'>
                                                {/* screen mobile preview */}

                                                <div className='wrap-checkin' style={{ backgroundColor: backgroundColor }}>
                                                    <div className='information-reward'>
                                                        <div className='wrap-star'>
                                                            <i className="fas fa-star"></i>
                                                            <i className="fas fa-star"></i>
                                                            <i className="fas fa-star"></i>
                                                        </div>
                                                        <img alt='logo-menchies' src={urlLogo} width={widthLogo} />
                                                        <h1 style={{ color: textColor }} >{mainHeading}</h1>
                                                        <p style={{ color: textColor }}>{mainSubHeading}</p>
                                                        <button>Views Details</button>
                                                    </div>
                                                    <div className='enter-phone' style={{ backgroundColor: primaryColor }} >
                                                        <h4>Enter your  Number</h4>
                                                        <div className='form-input-phone'>
                                                            <input type='text' disabled />
                                                            <i className="fas fa-backspace"></i>
                                                        </div>

                                                        <div className='group-number-phone'>
                                                            <span >1</span>
                                                            <span >2</span>
                                                            <span >3</span>
                                                            <span >4</span>
                                                            <span >5</span>
                                                            <span >6</span>
                                                            <span >7</span>
                                                            <span >8</span>
                                                            <span >9</span>
                                                            <span >0</span>
                                                        </div>
                                                        <button style={{ backgroundColor: buttonColor }} >Check-In</button>
                                                    </div>
                                                </div>
                                                {/* screen mobile preview */}

                                            </div>
                                        )
                                        : (
                                            <div className='no-screen-checkin'>
                                                <i className="fas fa-tablet-alt"></i>
                                                <h3>No Check-In Screen Selected ...</h3>
                                                <p>Click the <i className="fas fa-cog"></i> icon on a check-in screen to preview it's design here</p>
                                            </div>
                                        )
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <Loading showLoading={this.state.showLoading} />
            </Fragment>
        )
    }
}

const mapStateToProps = ( {addLoyalty} :  AddLoyaltyProps) => {
    return {
        status: addLoyalty.status,
        response: addLoyalty.response
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        getListScreenName: () => {
            dispatch(getListScreenName())
        },
        getListScreenDevice: () => {
            dispatch(getListScreenDevice())
        },
        addProgram: (data: any) => {
            dispatch(addProgram(data))
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddLoyalty);
