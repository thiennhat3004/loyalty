import React, { Fragment, Dispatch } from "react";
import './styles.scss';
import { Link } from "react-router-dom";
import {
    connect
} from 'react-redux';

const arrItemReward: any = [
    // {
    //     id: 123,
    //     points: 10,
    //     name: 'FREE 16oz',
    //     description: 'FREE 16oz coffee when 10 or more accumulated points occurs'
    // }
];

interface State {
    [key: string]: any,
    listItemReward: Array<any>,
    id: any,
    name: string,
    description: string,
    points: any,
}

interface Props {
    location: any,
    getValuePackage: any,
    isPageAdd: boolean
}

class AddPackage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            isPageAdd: true,
            listItemReward: [],
            id: null,
            name: '',
            description: '',
            points: '',
        }
    }

    UNSAFE_componentWillMount() {
        const isPageAdd = this.props.isPageAdd;
        if (isPageAdd) {
            this.setState({
                isPageAdd: true
            })
        } else {
            this.setState({
                isPageAdd: false
            })
        }
        
    }

    componentDidMount() {
        this.setState({
            listItemReward: arrItemReward
        })
    }

    showContentReward(e: any, item: any) {
        const contentElem = e.target.parentNode.parentNode.nextSibling;
        const checkMh = contentElem.classList.contains('maxheight-auto');
        const contentRewardOption = document.querySelectorAll('.content-reward-option');
        const { id, name, description, points } = this.state;
        this.setState({
            id: item.id
        });
        if (checkMh) {
            contentElem.classList.remove('maxheight-auto');

        } else {
            contentRewardOption.forEach(val => {
                val.classList.remove('maxheight-auto');
            })
            contentElem.classList.add('maxheight-auto');
            
            this.setState({
                name: item.name,
                description: item.description,
                points: item.points
            })
        }
    }

    getFormReward(e: any, index: number, id: number) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        },() => {
            let objNew;
            if (id) {
                objNew = {
                    id: id,
                    name : this.state.name,
                    description: this.state.description,
                    points: this.state.points
                };
            } else {
                objNew = {
                    name : this.state.name,
                    description: this.state.description,
                    points: this.state.points
                };
            }
            
            this.state.listItemReward.splice(index, 1, objNew);
            console.log(this.state.listItemReward);
            
            this.props.getValuePackage(this.state.listItemReward);
        });
    }

    copyItemReward(item: any) {
        const arrNew = this.state.listItemReward;
        const objNew = Object.create(item);

        objNew.points = item.points;
        objNew.name = item.name;
        objNew.description = item.description;
        arrNew.push(objNew);

        this.setState({
            listItemReward: arrNew
        })
    }

    deleteItemReward(index: number) {
        console.log(index);

        this.state.listItemReward.splice(index, 1);
        this.setState({
            listItemReward: this.state.listItemReward
        })
    }

    addItemRewardEmpty() {
        const objEmpty = {
            points: 0,
            name: '',
            description: ''
        }
        const arrNew = this.state.listItemReward;
        arrNew.push(objEmpty);
        this.setState({
            listItemReward: arrNew
        })
    }

    render() {
        return (
            <div className="content-inner">
                <div>
                    <p className='tutorial-use-offer'>
                        Reward are the offers that people can redeem their points for. You can configure any reward   by clicking the <i className="fas fa-cog"></i> icon. You cac clone any reward y clicking the <i className="far fa-copy"></i> icon. To Delete one simply click the <i className="fas fa-trash-alt"></i> icon
                                                </p>
                    {/* item option reward */}
                    {
                        this.state.listItemReward.map((value, index) => {
                            return (
                                <div className='item-reward-option' key={index}>
                                    <div className='title-reward-option'>
                                        <div>
                                            <span>{value.points}</span>
                                            <i className="fas fa-star"></i>
                                        </div>
                                        <h4>
                                            <i className="fas fa-trophy"></i>
                                            {value.name}
                                        </h4>
                                        <ul>
                                            <li className="fas fa-cog" onClick={(e) => this.showContentReward(e, value)}></li>
                                            <li className="far fa-copy" onClick={() => this.copyItemReward(value)}></li>
                                            <li className="fas fa-trash-alt" onClick={() => this.deleteItemReward(index)}></li>
                                        </ul>
                                    </div>
                                    <div className='content-reward-option'>
                                        <div className='input-option-program'>
                                            <label>Reward Name</label>
                                            <input type='text' className='form-text' name='name' value={value.name} onChange={(e) => this.getFormReward(e, index, value.id)} />
                                        </div>

                                        <div className='input-option-program'>
                                            <label>Reward Description</label>
                                            <textarea className='form-text' name='description' value={value.description} onChange={(e) => this.getFormReward(e, index, value.id)}>

                                            </textarea>
                                        </div>

                                        <div className='input-option-program'>
                                            <label>Points Cost</label>
                                            <input type='text' className='form-text' name='points' value={value.points} onChange={(e) => this.getFormReward(e, index, value.id)} />
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }

                    {/* item option reward */}
                    <div className='add-item-reward'>
                        <i className="fas fa-plus-circle"></i>
                        <span onClick={() => this.addItemRewardEmpty()}>Add Reward Offer</span>
                    </div>

                </div>
            </div>
        )
    }
}




const mapStateToProps = () => {
    return {
        
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPackage);