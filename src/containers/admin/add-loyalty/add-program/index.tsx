import React, { Fragment, useState, useEffect } from "react";
import './styles.scss';


interface Props {
    valueProgram: any
}

export function AddProgram(props: Props) {
    const [state, setState] = useState({
        nameProgram: '',
        desProgram: '',
        howReward: 1,
        timeCheckin: 0,
        businessName: ''
    });

    const getAddProgram = (e: any) => {
        const value = e.target.value;
        setState({
            ...state,
            [e.target.name] : value            
        });
        
    }

    useEffect(() => {
        props.valueProgram(state);
    }, [state])

    return (
        <div className="content-inner">

            <div className='input-option-program'>
                <label>Program name (people will not see)</label>
                <input type='text' className='form-text' name='nameProgram' onChange={getAddProgram} value={state.nameProgram} />
            </div>

            <div className='input-option-program'>
                <label>Program description (people will not see)</label>
                <textarea className='form-text' name='desProgram' onChange={getAddProgram} value={state.desProgram}>

                </textarea>
            </div>

            <div className='input-option-program'>
                <label>Business name</label>
                <input type='text' className='form-text' name='businessName' onChange={getAddProgram} value={state.businessName} />
            </div>

            <div className='input-option-program'>
                <label>How will  you reward people?</label>
                <select className='form-text' name='howReward' onChange={getAddProgram} value={state.howReward}>
                    <option value={1}>
                        Reward people for visit
                    </option>
                    <option value={2}>
                        Reward people for purchases
                    </option>
                </select>
            </div>

            <div className='input-option-program'>
                <label>How will  you reward people?</label>
                <div className='time-checkin'>
                    <input className='form-text' type='number' min='0' max='24' name='timeCheckin' onChange={getAddProgram} />
                    <select className='form-text'>
                        <option>
                            For All Check-In Screens
                        </option>
                    </select>
                </div>
            </div>

        </div>
    )
}