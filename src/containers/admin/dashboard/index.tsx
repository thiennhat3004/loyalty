import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import './styles.scss';
import { connect } from 'react-redux';
import { ACTIONS } from './../../../actions/contants/contants';
import { getNameDevice, updateNameDevice, getProgramOfMaster, deleteProgram } from './../../../actions/dashboard-admin';
import Modal from "../../../components/popup";
import Loading from "../../../components/loading";

interface DashboardProps  {
    history : {
        push(url: string) : void
    },
    dashboardAdmin: any,
    getNameDevice: any,
    updateNameDevice: any,
    getProgramOfMaster: any,
    deleteProgram: any
}

interface DashboardState {
    isShowAddNameDevice: boolean,
    nameDevice: string,
    modal: any,
    showLoading: boolean,
    listProgramMaster: any,
    isShowActionProgram: boolean,
    idProgramDelete: any
}

class Dashboard extends React.Component<DashboardProps, DashboardState> {

    constructor(props: DashboardProps) {
        super(props);
        this.state = {
            idProgramDelete: null,
            isShowAddNameDevice: false,
            nameDevice: '',
            modal : {
                showModal: false,
                titleModal: '',
                contentModal: '',
                isNotification: true,
                textAccept: ''
            },
            listProgramMaster: [],
            showLoading :false,
            isShowActionProgram: false
        }
    }

    componentDidMount() {
        this.setState({showLoading: true});
        this.props.getProgramOfMaster();
    }

    componentWillReceiveProps(nextProps: any) {
        if (nextProps) {
            if (nextProps.status === ACTIONS.GET_PROGRAM_MASTER_SUCCESS) {
                this.setState({
                    showLoading: false,
                    listProgramMaster: nextProps.response
                })
            }
        }
    }
    
    navigateAddProgram() {
        let address_device = localStorage.getItem('address_device');
        this.setState({showLoading: true});
        if (address_device) {
           let address = JSON.parse(address_device);
            let tokenAddress = address.token_address;
            let data = {
                token_address : tokenAddress
            }
            this.props.getNameDevice(data);
            console.log(data);
        }
        
    }

    UNSAFE_componentWillReceiveProps(nextProps: any) {
        if (nextProps) {
            if (nextProps.status === ACTIONS.GET_NAME_DEVICE_SUCCESS) {
                console.log(nextProps.response);
                if (nextProps.response.token_address) {
                    localStorage.setItem('address_device', JSON.stringify(nextProps.response));
                }
                
                if (!nextProps.response.name) {
                    this.setState({
                        isShowAddNameDevice: true,
                        showLoading: false
                    })
                } else {
                    console.log('chayj phan nay');
                    
                    this.props.history.push(`/admin/add-program`);
                }
            }

            if (nextProps.status === ACTIONS.UPDATE_NAME_DEVICE_SUCCESS) {
                console.log(nextProps.response);
                this.setState({
                    modal: {
                        showModal: true,
                        titleModal: '',
                        contentModal: nextProps.response.message,
                        isNotification: true,
                    },
                    showLoading: false
                })
            }

            if (nextProps.status === ACTIONS.DELETE_PROGRAM_SUCCESS) {
                console.log('delete ok');
                let arrNew = this.state.listProgramMaster.filter((val: any) => {
                    return val.id != this.state.idProgramDelete;
                })

                this.setState({
                    showLoading: false,
                    listProgramMaster: arrNew
                })
                
            }
        }
    }

    getNameDevice(e: any) {
        let nameDevice = e.target.value;
        this.setState({
            nameDevice
        })
    }

    updateNameDevice() {
        console.log(this.state.nameDevice);
        let device_id = localStorage.getItem('address_device');
        if (device_id) {
            let data = {
                device_id : JSON.parse(device_id).id,
                name: this.state.nameDevice
            }
            this.setState({
                isShowAddNameDevice: false,
                showLoading: true
            })
            this.props.updateNameDevice(data);
        }
        
    }

    onCloseModal() {
        this.setState({
            modal : {
                showModal: false
            }
        })
    }

    deleteProgram(id: number) {
        this.setState({
            isShowActionProgram: false,
            idProgramDelete: id,
            modal: {
                indexModal: 1,
                titleModal: '',
                showModal: true,
                contentModal: '',
                isNotification: false,
                textAccept: 'Delete'
            }
        });
            
        
    }

    actionModal() {
        if (this.state.modal.indexModal == 1) {
            this.setState({
                showLoading: true
            })
            this.props.deleteProgram(this.state.idProgramDelete);
        }
    }

    render() {
        return(
            <Fragment>
                <div className='name-aplication'>
                            <h1>My Loyalty Programs</h1>
                            <div>
                                <p>You have 1 Loyalty program created</p>
                                <button onClick={() => this.navigateAddProgram()}>Create A New Loyalty Program</button>
                            </div>

                        </div>
                <div className='total-loyalty-program'>
                            <table>
                                <thead>
                                    <tr>
                                        <th>
                                            PROGRAM NAME
                                        </th>
                                        <th>
                                            DESCRIPTION
                                        </th>
                                        <th>
                                            PROGRAM TYPE
                                        </th>
                                        <th>
                                            <i className="fas fa-users"></i>
                                        </th>
                                        <th>
                                            <i className="fas fa-cog"></i>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {
                                        this.state.listProgramMaster.map((value: any, index: number) => {
                                            return(
                                                <tr key={index}>
                                                    <td>{ value.name }</td>
                                                    <td>{ value.description }</td>
                                                    <td>{ value.how_reward == 1 ? 'Reward people for visit' : 'Reward people for purchases'  }</td>
                                                    <td><Link to='/admin/members'>1 Members</Link></td>
                                                    <td>
                                                       <div className='action-list-program' onClick={() => {this.setState({isShowActionProgram: !this.state.isShowActionProgram})}}>
                                                           <span>Action</span>
                                                           {
                                                               this.state.isShowActionProgram
                                                                ? <ul>
                                                                    <li>Edit</li>
                                                                    <li onClick={() => this.deleteProgram(value.id)}>Delete</li>
                                                                </ul>
                                                                : null
                                                           }
                                                       </div>
                                                        {/* <select>
                                                            <option>Edit</option>
                                                            <option onClick={() => this.deleteProgram(value.id)}>Delete</option>
                                                        </select> */}
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                    
                                </tbody>
                            </table>
                        </div>
            
                {/* component add name device */}
                {
                    this.state.isShowAddNameDevice
                        ?    <div className='add-name-device'>
                                <div className='add-device'>
                                    <h4>Notification</h4>
                                    <div>
                                        <p>You must install the current store name to add programs!</p>
                                        <input type='text' placeholder='Enter the store name' name='nameDevice' onChange={(e) => this.getNameDevice(e)} />
                                    </div>
                                    <div className='btn-add-device'>
                                        <button className='cancel' onClick={() => {this.setState({isShowAddNameDevice: false})}}>Cancel</button>
                                        <button onClick={() => this.updateNameDevice()}>Add</button>
                                    </div>
                                </div>
                            </div>
                        :   null    
            }

            <Loading showLoading={this.state.showLoading} />

            <Modal 
                title={this.state.modal.titleModal} 
                showModal={ this.state.modal.showModal } 
                content={this.state.modal.contentModal} 
                isNotification={this.state.modal.isNotification}
                acceptAction={() => this.actionModal()}
                textAccept={this.state.modal.textAccept}
                onCloseModal={() => this.onCloseModal()}
            />
            </Fragment>
        )
    }
}

const mapStateToProps = ( {dashboardAdmin} :  DashboardProps) => {
    return {
        status: dashboardAdmin.status,
        response: dashboardAdmin.response
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        getNameDevice: (address: any) => {
            dispatch(getNameDevice(address))
        },
        updateNameDevice: (data: any) => {
            dispatch(updateNameDevice(data))
        },
        getProgramOfMaster: () => {
            dispatch(getProgramOfMaster())
        },
        deleteProgram: (id: number) => {
            dispatch(deleteProgram(id))
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
