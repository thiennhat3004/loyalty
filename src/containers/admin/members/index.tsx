import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import './styles.scss';
interface MembersProps  {
    history : {
        push(url: string) : void
    }
}

interface MembersState {
    isShowOptionMember : boolean
}

class Members extends React.Component<MembersProps, MembersState> {

    constructor(props: MembersProps) {
        super(props);
        this.state = {
            isShowOptionMember : false
        }
    }
    
    changeShowOptionMember() {
        this.setState({
            isShowOptionMember : !this.state.isShowOptionMember
        })
    }   
    
    navigateManageMember() {
        this.props.history.push(`/admin/members/manage`);
    }

    render() {
        return(
            <Fragment>
                <div className='wrap-members'>
                    <div className='title-option-members'>
                        <h1>Menchies Loyalty Members</h1>
                        <div>
                            <div>
                                <label>Sort By</label>
                                <input type='date' />
                            </div>
                            <div>
                                <label>Search</label>
                                <input type='text' placeholder='Search...' />
                            </div>
                            <button className='search-member'>Search</button>
                            <button className='clear-search-member'>Clear</button>
                            <button >View Loyalty Programs</button>
                        </div>
                    </div>

                    <div className='content-list-members'>
                        <table> 
                            <thead>
                                <tr>
                                    <th>NAME</th>
                                    <th>NUMBER</th>
                                    <th>BALANCE</th>
                                    <th>JOINED</th>
                                    <th>LAST SEEN</th>
                                    <th>
                                        <i className='fas fa-cog'></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Bobby</td>
                                    <td>716-345-8765</td>
                                    <td>
                                        <div className='balance-members'>
                                            <i className='fas fa-star'></i>
                                            1
                                        </div>
                                    </td>
                                    <td>Jul 24th @ 2:33 pm</td>
                                    <td>1 minutes ago</td>
                                    <td>
                                        <div className='option-member'>
                                            <div onClick={() => this.changeShowOptionMember()}>Option</div>
                                            {
                                                this.state.isShowOptionMember
                                                    ? <ul>
                                                        <li onClick={() => this.navigateManageMember()}><i className='fas fa-user' ></i>Manage member</li>
                                                        <li><i className='fas fa-trash-alt'></i>Delete member</li>
                                                    </ul>
                                                    : null
                                            }
                                        </div>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Members;