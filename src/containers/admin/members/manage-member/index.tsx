import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import './styles.scss';

const memberActive = [
    {
        titleActive : 'Reward Redeemed',
        contentActive : 'Thien nhat use 2 points to redeem a FREE 16oz bychecking in on the JameStone Scree',
        timeActive : 'Jul 24th @ 2:36 pm',
        pointsActive : 24
    },
    {
        titleActive : 'Reward Redeemed',
        contentActive : 'Thanh viet use 2 points to redeem a FREE 16oz bychecking in on the JameStone Scree',
        timeActive : 'Jul 24th @ 2:36 pm',
        pointsActive : 25
    },
    {
        titleActive : 'Reward Redeemed',
        contentActive : 'thien bao use 2 points to redeem a FREE 16oz bychecking in on the JameStone Scree',
        timeActive : 'Jul 24th @ 2:36 pm',
        pointsActive : 26
    },
    {
        titleActive : 'Reward Redeemed',
        contentActive : 'Thanh tan use 2 points to redeem a FREE 16oz bychecking in on the JameStone Scree',
        timeActive : 'Jul 24th @ 2:36 pm',
        pointsActive : 28
    },
    {
        titleActive : 'Reward Redeemed',
        contentActive : 'tran nhut use 2 points to redeem a FREE 16oz bychecking in on the JameStone Scree',
        timeActive : 'Jul 24th @ 2:36 pm',
        pointsActive : 24
    }
]

interface ManageMemberProps  {
    history : {
        push(url: string) : void
    }
}

interface ManageMemberState {
    isShowOptionMember : boolean,
    listMemberActive : Array<any>
}

class ManageMember extends React.Component<ManageMemberProps, ManageMemberState> {

    constructor(props: ManageMemberProps) {
        super(props);
        this.state = {
            isShowOptionMember : false,
            listMemberActive : []
        }
    }

    // UNSAFE_componentWillMount() {
        
    // }   
    
    componentDidMount() {
        
        this.setState({
            listMemberActive : memberActive
        }, () => {
            this.styleItemActiveMember();
        })
    }   

    styleItemActiveMember() {

        let itemMilestonesMember = document.querySelectorAll('.item-milestones-member');
        console.log(itemMilestonesMember);
        
        itemMilestonesMember.forEach((element) => {
            if (element instanceof HTMLElement) {
                let marginBottom = 30;
                let heightElem = element.offsetHeight / 2 + marginBottom;
                let spacing = element.offsetHeight / 2 + 17;
               console.log(heightElem);
               console.log(spacing);
               
               
                element.style.setProperty('--heightMilestones', `${heightElem}px`);
                element.style.setProperty('--spacingMilestones', `${spacing}px`);
            }
        });
    }

    showEditPointMember(e: any) {
        const elem = e.target.parentNode.parentNode.nextSibling;
        elem.classList.add('height-point-member');
        this.styleItemActiveMember();
    }

    changePointsMember(e: any, index: number) : void {
        console.log(e.target.value);
        const listActiveNew = this.state.listMemberActive.map((member, i) => {
            if (index !== i) return member;
            return { ...member, pointsActive : e.target.value };
          });
      
          this.setState({ listMemberActive: listActiveNew });
        
    }

    hideEditPointMember(e: any) {
        const elem = e.target.parentNode.parentNode;
        elem.classList.remove('height-point-member');
        this.styleItemActiveMember();
    }

    deleteActiveMember(index: number) {
        let arrNewDelete = [];
        arrNewDelete = this.state.listMemberActive.filter((val, i) => {
            console.log(i);
            return i !== index;
        })
        this.setState({
            listMemberActive : arrNewDelete
        })
    }

    render() {
        return(
            <Fragment>
                <div className='wrap-manage-member'>
                    <div className='manage-member'>
                        <h3><i className='fas fa-user'></i>Manage Loyalty Member</h3>
                        <div>
                            <div className='points-balance'>
                                <p><i className='fas fa-star'></i>1</p>
                                <span>Points Balance</span>
                            </div>
                            <div className='last-seen'>
                                <p><i className='far fa-eye'></i>1 Minute ago</p>
                                <span>Last seen</span>
                            </div>
                        </div>
                        <div className='form-name-member'>
                            <label>First Name</label>
                            <input type='text' />
                        </div>
                        <div className='form-phone-member'>
                            <label>Mobile Number</label>
                            <input type='text' />
                        </div>
                        <div className='btn-manage-member'>
                            <button>Cancel</button>
                            <button>Save</button>
                        </div>
                    </div>
                    <div className='active-member'>
                        <div className='title-member-active'>
                            <h4>Member Active</h4>
                            <h5>Add Activity<i className="fas fa-plus"></i></h5>
                        </div>
                        <div className='content-member-active'>
                            {
                                this.state.listMemberActive.map((value, index) => {
                                    return (
                                        <div key={index}>
                                            <i className='fas fa-trophy'></i>
                                            <div className='item-milestones-member'>
                                                <div>
                                                    <h5>{ value.titleActive }</h5>
                                                    <small>{ value.timeActive }</small>
                                                </div>
                                                <div>   
                                                    <p>{ value.contentActive }</p>
                                                    <div>
                                                        <i className="fas fa-pen" onClick={(e) => this.showEditPointMember(e)}></i>
                                                        <i className="fas fa-trash-alt" onClick={() => this.deleteActiveMember(index)}></i>
                                                    </div>
                                                </div>
                                                <div className='edit-points-member'>
                                                    <label>Points</label>
                                                    <div>
                                                        <input type='text' value={value.pointsActive} onChange={(e) => this.changePointsMember(e, index)} />
                                                        <button onClick={(e) => this.hideEditPointMember(e)}>Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default ManageMember;