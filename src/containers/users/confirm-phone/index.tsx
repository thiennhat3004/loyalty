import React, { Fragment }  from "react";
import { connect } from 'react-redux';
import './styles.scss';
import { addUser } from './../../../actions/add-user';
import { ACTIONS } from "../../../actions/contants/contants";
import Loading from "../../../components/loading";

interface Props {
    history : {
        push(url : string) : void
    },
    match: {
        params: any
    },
    addUser: any,
    infoAddUser: any
}

interface MyState  {
    isShowConfirmName : boolean,
    name: string,
    showLoading: boolean
}

class ConfirmPhone extends React.Component<Props, MyState> {
    
    constructor(props: Props) {
        super(props);
        this.state = {
            isShowConfirmName : false,
            name: '',
            showLoading: false
        }
    }

   
    navigateConfirmName = () => {
        // this.props.history.push('/earn-reward');
        this.setState({
            isShowConfirmName : true
        })
    }   
   
    navigateEnterPhone = () => {
        this.props.history.push(`/`);
    }

    navigateEarnReward = () => {
        this.setState({
            showLoading: true
        });
        let phone = this.props.match.params.phone;
        const device = localStorage.getItem('address_device');
        let idDevice = device !== null ? JSON.parse(device)['id'] : false;
        const bodyUserNew = {
            phone: +phone,
            device_id: idDevice,
            name: this.state.name
        }
        console.log(bodyUserNew);
        
        this.props.addUser(bodyUserNew);
        
    }

    getNameUser(e: any) {
        this.setState({
            name: e.target.value
        })
    }

    UNSAFE_componentWillReceiveProps(nextProps: any) {
        if (nextProps) {
            if (nextProps.status === ACTIONS.ADD_USER_SUCCESS) {
                if (nextProps.response.user_id) {
                    console.log(nextProps.response);
                    this.props.history.push(`/earn-reward?user_id=${nextProps.response.user_id}&group_id=${nextProps.response.group_id}`);
                }
            }
        }
    }

    render() {
        let phone = this.props.match.params.phone;
        return(
            <Fragment>
                
                {
                    this.state.isShowConfirmName
                        ? <div className='confirm-name'>
                        <h1>What is your firstname ?</h1>
                        <input 
                            type='text' 
                            placeholder='Tab to add name ...' 
                            value={this.state.name}
                            onChange={(e) => this.getNameUser(e)}
                        />
                        {/* <h1>What is your email ?</h1>
                        <input type='text' placeholder='Tab to add email ...' /> */}
                        <button onClick={() => this.navigateEarnReward()}>Complete signup</button>
                    </div>
                    : <div className='confirm-phone'>
                        <h1>Is this number correct ?</h1>
                        <p >{ phone }</p>
                        <button onClick={() => this.navigateConfirmName()}>Look's good</button>
                        <p onClick={() => this.navigateEnterPhone()}>No It's Wrong ...    </p>
                    </div>
                }
                
                <Loading showLoading={this.state.showLoading} />
            </Fragment>
            
        )
    }
}


const mapStateToProps = ({ infoAddUser }: Props) => {
    return {
        status: infoAddUser.status,
        response: infoAddUser.response
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        addUser: (body: any) => {
            dispatch(addUser(body))
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(ConfirmPhone);
