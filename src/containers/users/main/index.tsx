import React, { Fragment }  from "react";
import { connect } from 'react-redux';
import { getLayoutScreen, checkNumberPhone } from './../../../actions/screen-user';
import { ACTIONS } from './../../../actions/contants/contants';
import './styles.scss';
import Loading from "../../../components/loading";
interface UserProps {
    phone : Number,
    history : {
        push(url : string) : void
    },
    getLayoutScreen: any,
    screenUser: any,
    checkNumberPhone: any
}

interface MyState {
    phone : number,
    isShowPopupDetail : boolean,
    screen_info: any,
    enableCheckin :boolean,
    showLoading: boolean
}

class Main extends React.Component<UserProps, MyState> {
    
    constructor(props: UserProps) {
        super(props);
        this.state = {
            showLoading: false,
            phone : 0,
            isShowPopupDetail : true,
            enableCheckin: true,
            screen_info: {
                bg_color: '',
                main_heading: "FREE 16oz",
                sub_heading: "Recive 2 FREE oz and 1 point when you join now.It\"s free to get started and only takes a second!",
                message: "g",
                logo_url: "/images/logo_menchies.png",
                logo_width: 116,
                text_color: "#ffffff",
                primary_color: "#17176b",
                btn_color: "#1a96bb",
                btn_text_color: "#ffffff",
                icon_color: "#ffffff",
            }
        }
    }

    closeDetailProgram = () => {
        this.setState({
            isShowPopupDetail : false
        })
    }

    pressNumberPhone = (num: any) => {
        let phone = this.state.phone.toString();
        
        if (num === 'empty') {
            this.setState({
                phone : 0
            })
        } else if (num === 'delete') {
            phone = phone.substring(0, phone.length - 1)
            console.log(phone);
            
            this.setState({
                phone : +phone
            })  
            if (phone.length < 10) {
                this.setState({
                    enableCheckin: true
                })  
            }
        } else  {
            phone += num;
            if (phone.length < 11) {
                this.setState({
                    phone : +phone
                })
            }
            if (phone.length > 9 ) {
                this.setState({
                    enableCheckin: false
                })
            }
            
        }
        
    }

    submitCheckin = () => {
        console.log(this.state.phone);
        this.setState({
            showLoading: true
        })
        const device = localStorage.getItem('address_device');
        let idDevice = device !== null ? JSON.parse(device)['id'] : false;
        let body = {
            phone: this.state.phone,
            device_id: idDevice
        }
        this.props.checkNumberPhone(body);
    }

    componentDidMount() {
        this.setState({
            showLoading: true
        });
        const device = localStorage.getItem('address_device');
        let idDevice = device !== null ? JSON.parse(device)['id'] : false;
        
        this.props.getLayoutScreen(idDevice);
        
    }

    UNSAFE_componentWillReceiveProps(nextProps: any) {
        if (nextProps) {
            if (nextProps.status === ACTIONS.GET_SCREEN_DEVICE_SUCCESS) {
                console.log(nextProps.response);
                const { 
                    bg_color,
                    main_heading,
                    sub_heading,
                    message,
                    logo_url,
                    logo_width,
                    text_color,
                    primary_color,
                    btn_color,
                    btn_text_color,
                    icon_color,
                 } = nextProps.response;
                this.setState({
                    showLoading: false,
                    screen_info: {
                        bg_color,
                        main_heading,
                        sub_heading,
                        message,
                        logo_url,
                        logo_width,
                        text_color,
                        primary_color,
                        btn_color,
                        btn_text_color,
                        icon_color
                    }
                })
            }

            if (nextProps.status === ACTIONS.CHECK_PHONE_SUCCESS) {
                console.log(nextProps.response)
                if (nextProps.response.message) {
                    this.props.history.push(`/earn-reward?user_id=${nextProps.response.user_id}&group_id=${nextProps.response.group_id}`);
                } else {
                    this.props.history.push(`/confirm-phone/${nextProps.response.phone}`)
                }
            }
        }
    }

    render() {
        const {
            screen_info

        } = this.state;
        return(
            <Fragment>
                {
                    this.state.isShowPopupDetail
                        ? <div className='wrap-detail-program'>
                        <div className='popup-detail-program'>
                            <div className='wrap-star'>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                            </div>
                            <h1>Loyalty Program Details</h1>
                            <ul>
                                <li> <i className="fas fa-check"></i>You earn points for each visit</li>
                                <li> <i className="fas fa-check"></i>You earn points for each visit</li>
                                <li> <i className="fas fa-check"></i>You earn points for each visit</li>
                                <li> <i className="fas fa-check"></i>You earn points for each visit</li>
                            </ul>
    
                            <ul>
                                <li> <i className="fas fa-info-circle"></i>Tab a top corner of the screen to go back to slick text</li>
                                <li> <i className="fas fa-info-circle"></i>Tab a top corner of the screen to go back to slick text</li>
                                <li> <i className="fas fa-info-circle"></i>Tab a top corner of the screen to go back to slick text</li>
                            </ul>
                            <button onClick={() => this.closeDetailProgram()}>Close</button>
                        </div>
                    </div>
                    : null
                }
                
                <div className='wrap-checkin' style={{ backgroundColor: screen_info.bg_color }}>
                    <div className='information-reward'>
                        <div className='wrap-star'>
                            <i className="fas fa-star"></i>
                            <i className="fas fa-star"></i>
                            <i className="fas fa-star"></i>
                        </div>
                        <img alt='logo-menchies' src={ screen_info.logo_url } width={screen_info.logo_width} />
                        <h1 style={{ color: screen_info.text_color }}>{ screen_info.main_heading }</h1>
                        <p style={{ color: screen_info.text_color }}>{ screen_info.sub_heading }</p>
                        <button>Views Details</button>
                    </div>
                    <div className='enter-phone' style={{ backgroundColor: screen_info.primary_color }}>
                        <h4>Enter your  Number</h4>
                        <div className='form-input-phone'>
                            <input name="phone" pattern="^\d{10}$" type="text" disabled value={ this.state.phone } />
                            {/* <input type='text' disabled value={ this.state.phone }  /> */}
                            <i onClick={() => this.pressNumberPhone('delete')} className="fas fa-backspace"></i>
                        </div>
                        
                        <div className='group-number-phone'>
                            <span onClick={() => this.pressNumberPhone(1)}>1</span>
                            <span onClick={() => this.pressNumberPhone(2)}>2</span>
                            <span onClick={() => this.pressNumberPhone(3)}>3</span>
                            <span onClick={() => this.pressNumberPhone(4)}>4</span>
                            <span onClick={() => this.pressNumberPhone(5)}>5</span>
                            <span onClick={() => this.pressNumberPhone(6)}>6</span>
                            <span onClick={() => this.pressNumberPhone(7)}>7</span>
                            <span onClick={() => this.pressNumberPhone(8)}>8</span>
                            <span onClick={() => this.pressNumberPhone(9)}>9</span>
                            {/* <span onClick={() => this.pressNumberPhone('empty')}>empty</span> */}
                            <span onClick={() => this.pressNumberPhone(0)}>0</span>
                            {/* <span onClick={() => this.pressNumberPhone('delete')}> AC </span> */}
                        </div>
                        <button 
                            onClick={ () => this.submitCheckin() } 
                            style={{ backgroundColor: screen_info.btn_color }} 
                            disabled={this.state.enableCheckin}
                        >Check-In</button>
                    </div>
                </div>

                <Loading showLoading={this.state.showLoading} />
            </Fragment>
            
        )
    }
}

const mapStateToProps = ({ screenUser }: UserProps) => {
    return {
        status: screenUser.status,
        response: screenUser.response
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        getLayoutScreen: (id: number) => {
            dispatch(getLayoutScreen(id))
        },
        checkNumberPhone: (body: object) => {
            dispatch(checkNumberPhone(body))
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(Main);
