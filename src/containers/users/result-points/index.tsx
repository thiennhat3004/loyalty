import React, { Fragment }  from "react";
import './styles.scss';
import AutoRemove from "../../../components/auto-remove";
import * as queryString from 'query-string';

interface Props {
    history : {
        push(url : string) : void
    },
    location: {
        search: any
    }
}

interface MyState  {
    name: any,
    phone: any,
    score: any
}

class ResulPoints extends React.Component<Props, MyState> {
    
    constructor(props: Props) {
        super(props);
        this.state = {
            name: '',
            phone: '',
            score: 0
        }
    }

    navigateEnterPhone = () => {
        this.props.history.push(`/`);
    }

    componentDidMount() {
        let query = this.props.location.search;
        const result_user = queryString.parse(query);
        console.log(result_user);
        this.setState({
            name: result_user.name ? result_user.name : '',
            phone: result_user.phone,
            score: result_user.score
        })
        
    }
  

    render() {
        const { name, phone, score } = this.state;
        return(
            
            <Fragment>
                <div className='wrap-result-points'>
                    <div className='wrap-star'>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                    </div>

                    <h1>You've earned { score } ponit!</h1>

                    <div className='info-point-user'>
                        <h4>Points balance</h4>
                        <div>
                            <img src='/images/circle_star.png' alt='star big' />
                            <div>
                                <h2>{score} Points</h2>
                                <p>You have a reward to you on your next visit</p>
                            </div>
                        </div>
                        <div>
                            <span>{ name }</span>
                            <span>{ phone }</span>
                        </div>
                    </div>
                    <button onClick={ () => this.navigateEnterPhone() }>
                        Done
                    </button>    
                </div>
                
                {/* component auto navigate */}
                {/* <AutoRemove /> */}
            </Fragment>
            
        )
    }
}

export default ResulPoints;