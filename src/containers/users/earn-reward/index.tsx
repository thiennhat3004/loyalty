import React, { Fragment }  from "react";
import './styles.scss';
import  * as queryString from 'query-string';
import { connect } from 'react-redux';
import { getScoreUserOfGroup, getAllPackageDevice } from '../../../actions/earn-reward';
import { ACTIONS } from './../../../actions/contants/contants';
import Loading from "../../../components/loading";

const arrResult = [
    {
        point : 2,
        name : 'FREE 16 oz',
        description : 'FREE 16oz coffe when 10 or more accumulated points occur',
        active : false
    },
    {
        point : 5,
        name : 'FREE 16 oz',
        description : 'FREE 16oz coffe when 10 or more accumulated points occur',
        active : false
    },
    {
        point : 7,
        name : 'FREE 16 oz',
        description : 'FREE 16oz coffe when 10 or more accumulated points occur',
        active : false
    },
    {
        point : 8,
        name : 'FREE 16 oz',
        description : 'FREE 16oz coffe when 10 or more accumulated points occur',
        active : false
    }
];
interface MyProps {
    history : {
        push(url : string) : void
    },
    location: {
        search: any
    },
    earnReward: any,
    getScoreUserOfGroup: any,
    getAllPackageDevice: any
}

interface MyState {
    pointOfUser : number,
    allPackageDevice: Array<any>,
    showLoading: boolean,
    user_info: any,
    isDisableReward: boolean
}

class EarnReward extends React.Component<MyProps, MyState> {
    
    constructor(props: MyProps) {
        super(props);
        this.state = {
            pointOfUser : 0,
            allPackageDevice: [],
            showLoading: false,
            user_info: null,
            isDisableReward: true
        }
    }

    navigateResultPoints = () => {
        const { name, phone } = this.state.user_info;
        
        this.props.history.push(`/result-points?name=${name}&phone=${phone}&score=${this.state.pointOfUser}`);
    }

    navigateConfirmReward = () => {
        this.props.history.push(`/confirm-reward${this.props.location.search}`);
    }

    componentDidMount() {
        this.setState({
            showLoading: true
        });
        const query = queryString.parse(this.props.location.search);
        console.log(query);
        const body = {
            user_id: query.user_id,
            group_id: query.group_id
        };
        this.props.getScoreUserOfGroup(body);
        this.props.getAllPackageDevice(query.group_id);
    }

    UNSAFE_componentWillReceiveProps(nextProps: any) {
        if (nextProps) {
            if (nextProps.status === ACTIONS.GET_SCORE_USER_SUCCESS) {
                console.log(nextProps.response);
                this.setState({
                    pointOfUser: nextProps.response.score,
                    user_info: nextProps.response.user_info
                })
            }

            if (nextProps.status === ACTIONS.GET_PACKAGE_DEVICE_SUCCESS) {
                console.log(nextProps.response);
                let arr: any = [];
                nextProps.response.forEach((val: any) => {
                    let obj = {
                        name: val.reward_name,
                        description: val.reward_description,
                        point: val.score
                    }
                    if (+val.score <= +this.state.pointOfUser) {
                        this.setState({
                            isDisableReward: false
                        })
                    }
                    
                    arr.push(obj);
                })
                this.setState({
                    allPackageDevice: arr,
                    showLoading: false
                })
            }
        }
    }

    render() {
        return (
            <Fragment>
                <div className='wrap-earn-reward'>
                    <h1>You have rewards to you!</h1>
                    
                    <div className='pointed'>
                        <i className="fas fa-star"></i>
                        <span>{ this.state.pointOfUser } Points</span>
                    </div>
                    
                    <div className='total-level-reward'>
                        {/* item reward */}
                        {
                            this.state.allPackageDevice.map((val, i) => {
                                if (val.point <= this.state.pointOfUser) {
                                    val['active'] = true;
                                }

                                if (val.active) {
                                    return(
                                        <div className='item-reward item-reward-active' key={i}>
                                            <div>
                                                <i className="fas fa-check"></i>
                                            </div>
                                            <div>
                                                <div>
                                                    <i className='fas fa-star'></i>
                                                    <span>{ val.point }</span>
                                                </div>
                                                <h4 className='name-level'>
                                                    { val.name }
                                                </h4>
                                                <p className='description-level'>
                                                    { val.description }
                                                </p>
                                                <div>
                                                    <i className="fas fa-lock-open"></i>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                } else {
                                    return(
                                        <div className='item-reward item-reward-disabled' key={i}>
                                            <div>
                                                <div>
                                                    <i className='fas fa-star'></i>
                                                    <span>{ val.point }</span>
                                                </div>
                                                <h4 className='name-level'>
                                                    { val.name }
                                                </h4>
                                                <p className='description-level'>
                                                    { val.description }
                                                </p>
                                                <div>
                                                    <i className="fas fa-lock"></i>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                                
                            })    
                        }
                        
                        {/* item reward */}
                        
                    </div>

                    <div className='btn-reedeem-reward'>
                        <button 
                            disabled={this.state.isDisableReward} 
                            onClick={ () => this.navigateConfirmReward() }
                        >
                            Reedeem Reward
                        </button>
                    </div>
                    
                    <a onClick={ () => this.navigateResultPoints() }>
                      Save reward for later  
                    </a>
                </div>

                <Loading showLoading={this.state.showLoading} />
            </Fragment>
        )
    }
}


const mapStateToProps = ({ earnReward }: MyProps) => {
    return {
        status: earnReward.status,
        response: earnReward.response
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        getScoreUserOfGroup: (body: any) => {
            dispatch(getScoreUserOfGroup(body))
        },
        getAllPackageDevice: (id: any) => {
            dispatch(getAllPackageDevice(id))
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(EarnReward);
