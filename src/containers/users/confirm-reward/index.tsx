import React, { Fragment }  from "react";
import './styles.scss';
import * as queryString from 'query-string';
import { connect } from 'react-redux';
import { ACTIONS } from './../../../actions/contants/contants';
import { resultFinallyReward, resetScoreReward } from './../../../actions/earn-reward';
import Loading from "../../../components/loading";

interface MyProps {
    history : {
        push(url : string) : void
    },
    location: {
        search: any
    },
    resultFinallyReward: any,
    earnReward :any,
    resetScoreReward: any
}

interface MyState {
    name: string,
    description: string,
    score: number,
    showLoading: boolean
}

class ConfirmReward extends React.Component<MyProps, MyState> {
    
    constructor(props: MyProps) {
        super(props);
        this.state = {
            name: '',
            description: '',
            score: 0,
            showLoading: false
        }
    }

    updateScoreAfterReward() {
        const query = queryString.parse(this.props.location.search);
        console.log(query);
        const body = {
            user_id: query.user_id,
            group_id: query.group_id,
            score: this.state.score
        };
        this.props.resetScoreReward(body);
    }

    componentDidMount() {
        this.setState({
            showLoading: true
        });
        const query = queryString.parse(this.props.location.search);
        console.log(query);
        const body = {
            user_id: query.user_id,
            group_id: query.group_id
        };
        this.props.resultFinallyReward(body);
    }    

    UNSAFE_componentWillReceiveProps(nextProps: any) {
        if (nextProps) {
            if (nextProps.status === ACTIONS.GET_REWARD_FINALLY_SUCCESS) {
                console.log(nextProps.response);
                this.setState({
                    name: nextProps.response.reward_name,
                    description: nextProps.response.reward_description,
                    score: nextProps.response.score,
                    showLoading: false
                })
            }

            if (nextProps.status === ACTIONS.RESET_SCORE_USER_SUCCESS) {
                console.log(nextProps.response);
                
            }
        }
    }

    render() {
        const { name, description, score } = this.state;
        return(
            <Fragment>
                <div className='confirm-reward'>
                    <h1>You have reedeemed this reward!</h1>
                    <p>Please show this screen to an employee</p>
                    <div className='total-reward'>
                        <div>
                            <i className="fas fa-star"></i>
                            <span>{ score }</span>
                        </div>
                        <h4>{ name }</h4>
                        <p>{ description }</p>
                        <div>
                            <i className="fas fa-trophy"></i>
                        </div>
                    </div>
                    <div className='btn-confirm-reward'>
                        <button onClick={() => this.updateScoreAfterReward()}>Done</button>
                    </div>
                </div>

                <Loading showLoading={this.state.showLoading} />
            </Fragment>
        )
    }
}


const mapStateToProps = ({ earnReward }: MyProps) => {
    return {
        status: earnReward.status,
        response: earnReward.response
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        resultFinallyReward: (body: any) => {
            dispatch(resultFinallyReward(body))
        },
        resetScoreReward: (body: any) => {
            dispatch(resetScoreReward(body))
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(ConfirmReward);
