import React from 'react';
import './App.scss';
import { BrowserRouter as Router, Route, Switch, Redirect  } from 'react-router-dom';
import Main from './containers/users/main';
import PageError from './containers/users/PageError';
import ConfirmPhone from './containers/users/confirm-phone';
import EarnReward from './containers/users/earn-reward';
import ResulPoints from './containers/users/result-points';
import AutoRemove from './components/auto-remove';
import ConfirmReward from './containers/users/confirm-reward';
import Admin from './containers/admin';
import Signup from './containers/auth/signup/index';
import ForgotPassword from './containers/auth/forgot-password';
import SuperAdmin from './containers/super-admin';
import Login from './containers/auth/login/index';
import { WaitAccept } from './containers/auth/wait-accept';
import { SentEmail } from './containers/auth/send-email/index';
import ResetPassword from './containers/auth/reset-password';

interface PrivateRouteProps {
    // tslint:disable-next-line:no-any
    component: any;
    // tslint:disable-next-line:no-any
    path : string;
    exact? : boolean;
    layout : any
}

interface LayoutProps {
    component : any;
    children : any
}

const AdminLayout = (props : LayoutProps) => {
    let { component: Component, ...rest } = props;  
    return (  
        <div className='content-admin'>
            {props.children}
        </div> 
    )  
  } 

  const UserLayout = (props : LayoutProps) => {
    let { component: Component, ...rest } = props;  
    return (  
        <div className='content-loyalty'>
            {props.children}
        </div>
    )  
  } 


const PrivateRoute = (props: PrivateRouteProps) => {
    let { component: Component, layout : Layout, ...rest } = props;
    const user = localStorage.getItem('USER_INFO');
    return(
    <Route {...rest} render={(props) => (
        user
            ? <Layout>
                <Component {...props} />
             </Layout>
            : <Layout>
                <Redirect to={{ pathname : `/login` }} />
            </Layout>
    )} />
    );
}

class  App extends React.Component {
    

    render() {
        return (
                <Router>
                    <Switch>
                            <Route layout={UserLayout} path={`/login`}  component={ Login } />        
                            <Route layout={UserLayout} path={`/signup`}  component={ Signup } />        
                            <Route layout={UserLayout} path={`/forgot-password`} exact component={ ForgotPassword } /> 
                            <Route layout={UserLayout} path={`/reset-password`} exact component={ ResetPassword } /> 
                            <Route layout={UserLayout} path={`/wait-accept`} exact component={ WaitAccept } /> 
                            <Route layout={UserLayout} path={`/sent-email`} exact component={ SentEmail } /> 
                            <PrivateRoute layout={UserLayout} path={`/`} exact={true} component={ Main }  />
                            <PrivateRoute layout={UserLayout} path={`/confirm-phone/:phone`}  component={ ConfirmPhone }  />
                            <PrivateRoute layout={UserLayout} path={`/earn-reward`}  component={ EarnReward }  />
                            <PrivateRoute layout={UserLayout} path={`/result-points`}  component={ ResulPoints }  />
                            <PrivateRoute layout={UserLayout} path={`/confirm-reward`}  component={ ConfirmReward }  />
                            <PrivateRoute layout={AdminLayout} path={`/admin`}  component={ Admin }  />
                            <PrivateRoute layout={AdminLayout} path={`/super-admin`}  component={ SuperAdmin }  />
                            <PrivateRoute layout={UserLayout} path={`*`} exact={true} component={ PageError }  />
                </Switch>
                </Router>
        )
    }
    
}

export default App;
