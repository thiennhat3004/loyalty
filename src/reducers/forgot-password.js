import { ACTIONS } from '../actions/contants/contants';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.SEND_FORGOT_PASSWORD_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.SEND_FORGOT_PASSWORD_ERROR:
            return {
                ...state,
                status: action.type,
                response: action.err,
            };
        default:
            return state;
    }
};
