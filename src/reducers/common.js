import { ACTIONS } from './../actions/contants/contants';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.GET_PROFILE_SUCCESS:
        case ACTIONS.LOGOUT_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.GET_PROFILE_ERROR:
        case ACTIONS.LOGOUT_ERROR:
            return {
                ...state,
                status: action.type,
                response: action.err,
            };
        default:
            return state;
    }
};
