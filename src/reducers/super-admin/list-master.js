import { ACTIONS } from '../../actions/super-admin/list-master';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.GET_LIST_MASTER_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.DELETE_MASTER_SUCCESS:
        case ACTIONS.SET_ROLE_MASTER_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        
        case ACTIONS.GET_LIST_MASTER_ERROR:
        case ACTIONS.DELETE_MASTER_ERROR:    
        case ACTIONS.SET_ROLE_MASTER_ERROR:
            return {
                ...state,
                status: action.type,
                response: action.err,
            };
        default:
            return state;
    }
};
