import { ACTIONS } from '../../actions/super-admin/super-admin';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.GET_PROFILE_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.GET_PROFILE_ERROR:
            return {
                ...state,
                status: action.type,
                response: action.err,
            };
        default:
            return state;
    }
};
