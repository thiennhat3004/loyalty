import { ACTIONS } from './../actions/contants/contants';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.ADD_USER_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.ADD_USER_ERROR:
            return {
                ...state,
                status: action.type,
                response: action.err,
            };
        default:
            return state;
    }
};
