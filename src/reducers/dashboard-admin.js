import { ACTIONS } from './../actions/contants/contants';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.GET_NAME_DEVICE_SUCCESS:
        case ACTIONS.UPDATE_NAME_DEVICE_SUCCESS:
        case ACTIONS.GET_PROGRAM_MASTER_SUCCESS:
        case ACTIONS.DELETE_PROGRAM_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.GET_NAME_DEVICE_ERROR:
        case ACTIONS.UPDATE_NAME_DEVICE_ERROR:
        case ACTIONS.GET_PROGRAM_MASTER_ERROR:
        case ACTIONS.DELETE_PROGRAM_ERROR:
            return {
                ...state,
                status: action.type,
                response: action.err,
            };
        default:
            return state;
    }
};
