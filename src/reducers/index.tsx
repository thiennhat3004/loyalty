import {
    combineReducers,
  } from 'redux';
import login from './login';
import signup from './signup';
import superAdmin from './super-admin/super-admin';
import listMaster from './super-admin/list-master';
import resetPassword from './reset-password';
import forgotPassword from './forgot-password';
import common from './common';
import dashboardAdmin from './dashboard-admin';
import addLoyalty from './add-loyalty';
import screenUser from './screen-user';
import infoAddUser from './add-user';
import earnReward from './earn-reward';

  const rootReducer = combineReducers({
    login, signup, superAdmin, listMaster, resetPassword,
    forgotPassword, dashboardAdmin, common, addLoyalty, screenUser,
    infoAddUser, earnReward
});
  
  export default rootReducer;
  