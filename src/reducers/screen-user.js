import { ACTIONS } from './../actions/contants/contants';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.GET_SCREEN_DEVICE_SUCCESS:
        case ACTIONS.CHECK_PHONE_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.GET_SCREEN_DEVICE_ERROR:
        case ACTIONS.CHECK_PHONE_ERROR:
            return {
                ...state,
                status: action.type,
                response: action.err,
            };
        default:
            return state;
    }
};
