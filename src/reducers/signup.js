import { ACTIONS } from '../actions/signup';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.SIGNUP_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.SIGNUP_ERROR:
            return {
                ...state,
                status: action.type,
                response: action.err,
            };
        default:
            return state;
    }
};
