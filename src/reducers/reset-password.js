import { ACTIONS } from '../actions/contants/contants';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.RESET_PASSWORD_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.RESET_PASSWORD_ERROR:
            return {
                ...state,
                status: action.type,
                error: action.data,
            };
        default:
            return state;
    }
};
