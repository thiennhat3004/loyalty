import { ACTIONS } from '../actions/contants/contants';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.ADD_PROGRAM_SUCCESS:
        case ACTIONS.GET_LIST_SCREEN_NAME_SUCCESS:
        case ACTIONS.GET_LIST_SCREEN_DEVICE_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.ADD_PROGRAM_ERROR:
        case ACTIONS.GET_LIST_SCREEN_NAME_ERROR:
        case ACTIONS.GET_LIST_SCREEN_DEVICE_ERROR:
            return {
                ...state,
                status: action.type,
                response: action.err,
            };
        default:
            return state;
    }
};
