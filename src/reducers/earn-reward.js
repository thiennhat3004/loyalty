import { ACTIONS } from '../actions/contants/contants';

export default (state = {}, action) => {
    switch (action.type) {
        case ACTIONS.GET_SCORE_USER_SUCCESS:
        case ACTIONS.GET_PACKAGE_DEVICE_SUCCESS:
        case ACTIONS.GET_REWARD_FINALLY_SUCCESS:
        case ACTIONS.RESET_SCORE_USER_SUCCESS:
            return {
                ...state,
                status: action.type,
                response: action.data,
            };
        case ACTIONS.GET_SCORE_USER_ERROR:
        case ACTIONS.GET_PACKAGE_DEVICE_ERROR:
        case ACTIONS.GET_REWARD_FINALLY_ERROR:
        case ACTIONS.RESET_SCORE_USER_ERROR:
            return {
                ...state,
                status: action.type,
                response: action.err,
            };
        default:
            return state;
    }
};
